<?php

use Illuminate\Database\Seeder;

class ProviciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        

		DB::table('provincias')->insert([
			['provcodigo' => '109', 'nombre' => 'GUAYAS', 'region' => '1', ],
			['provcodigo' => '110', 'nombre' => 'PICHINCHA', 'region' => '2', ],
			['provcodigo' => '111', 'nombre' => 'MANABI', 'region' => '1', ],
			['provcodigo' => '112', 'nombre' => 'AZUAY', 'region' => '2', ],
			['provcodigo' => '113', 'nombre' => 'BOLIVAR', 'region' => '2', ],
			['provcodigo' => '114', 'nombre' => 'CAÑAR', 'region' => '2', ],
			['provcodigo' => '115', 'nombre' => 'CARCHI', 'region' => '2', ],
			['provcodigo' => '116', 'nombre' => 'CHIMBORAZO', 'region' => '2', ],
			['provcodigo' => '117', 'nombre' => 'COTOPAXI', 'region' => '2', ],
			['provcodigo' => '118', 'nombre' => 'EL ORO', 'region' => '1', ],
			['provcodigo' => '119', 'nombre' => 'ESMERALDAS', 'region' => '1', ],
			['provcodigo' => '120', 'nombre' => 'GALAPAGOS', 'region' => '4', ],
			['provcodigo' => '121', 'nombre' => 'IMBABURA', 'region' => '2', ],
			['provcodigo' => '122', 'nombre' => 'LOJA', 'region' => '2', ],
			['provcodigo' => '123', 'nombre' => 'LOS RIOS', 'region' => '1', ],
			['provcodigo' => '124', 'nombre' => 'MORONA SANTIAGO', 'region' => '3', ],
			['provcodigo' => '125', 'nombre' => 'NAPO', 'region' => '3', ],
			['provcodigo' => '126', 'nombre' => 'ORELLANA', 'region' => '3', ],
			['provcodigo' => '127', 'nombre' => 'PASTAZA', 'region' => '3', ],
			['provcodigo' => '128', 'nombre' => 'SANTA ELENA', 'region' => '1', ],
			['provcodigo' => '129', 'nombre' => 'STO. DOMINGO DE LOS TSACHILAS', 'region' => '2', ],
			['provcodigo' => '130', 'nombre' => 'SUCUMBIOS', 'region' => '3', ],
			['provcodigo' => '131', 'nombre' => 'TUNGURAHUA', 'region' => '2', ],
			['provcodigo' => '132', 'nombre' => 'ZAMORA CHINCHIPE', 'region' => '3', ],
			['provcodigo' => '133', 'nombre' => 'AZUAY', 'region' => '2', ],
		]);
    }
}
