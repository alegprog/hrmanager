<?php

use Illuminate\Database\Seeder;

class ConceptosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('conceptos')->insert([
			['concodigo' => 'IOVAR017', 'descrip' => 'VARIOS', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'HEXTR050', 'descrip' => 'GRAT. DESEMPENO', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'HEXTR100', 'descrip' => 'GRAT. PRODUCCION', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR018', 'descrip' => 'VARIOS', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IESSEXTC', 'descrip' => 'COBERTURA CONYUGE', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR019', 'descrip' => 'ANTICIPO', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'BENDEC13', 'descrip' => 'DECIMO TERCER SUELDO', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'BENDEC14', 'descrip' => 'DECIMO CUARTO SUELDO', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'DECIMO13', 'descrip' => 'DEC. TERCER MENSUAL', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'DECIMO14', 'descrip' => 'DEC. CUARTO MENSUAL', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'DEC13PAG', 'descrip' => 'DECIMO TERCER SUELDO MENSUAL', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'DEC14PAG', 'descrip' => 'DECIMO CUARTO SUELDO MENSUAL', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR020', 'descrip' => 'CONT. OBLIGAT.', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'DES00001', 'descrip' => 'CONT. SOLIDARIA', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR021', 'descrip' => 'FIJO', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'BEN00001', 'descrip' => 'FONDOS DE RESERVA MENSUAL', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR001', 'descrip' => 'COMISION', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IESSPERS', 'descrip' => 'IESS 9.45%', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IESSPATR', 'descrip' => 'IESS APORTE PATRONAL', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR002', 'descrip' => 'BONO', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR003', 'descrip' => 'BONO EXCELENCIA', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'SUELDRM1', 'descrip' => 'SUELDO ROL MENSUAL', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'SUELDEM1', 'descrip' => 'SUELDO ROL MENSUAL', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'SUELDRQ1', 'descrip' => 'ANTICIPO QUINCENAL', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'SUELDEQ1', 'descrip' => 'ANTICIPO QUINCENAL', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'SUELDRQ2', 'descrip' => 'SUELDO MENSUAL', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'SUELDEQ2', 'descrip' => 'SUELDO MENSUAL EXTRA', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'ANTPA1NO', 'descrip' => 'ANTICIPO QUINCENAL', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'ANTPA1EX', 'descrip' => 'ANTICIPO QUINCENAL', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'DNOTVACA', 'descrip' => 'DESC x VACACION', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'PAGOVACA', 'descrip' => 'PAGO VACACIONES', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'ANTSUELD', 'descrip' => 'ANTICIPOS A SUELDO', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'PRESTAMO', 'descrip' => 'PRESTAMOS', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'DSFALTAS', 'descrip' => 'PERMISOS Y FALTAS', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR004', 'descrip' => 'SECTORIZADO', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR005', 'descrip' => 'HUMANA', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR006', 'descrip' => 'CELULAR', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'HELIQMAN', 'descrip' => 'LIQ. H.EXTRAS', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR007', 'descrip' => 'COMIDA', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR008', 'descrip' => 'DEUDA A EMPRESA', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR009', 'descrip' => 'PQ IESS', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR010', 'descrip' => 'PH IESS', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR011', 'descrip' => 'GENESIS', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR012', 'descrip' => 'FALTAS', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR013', 'descrip' => 'MULTAS', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR014', 'descrip' => 'IMPUESTO RENTA', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR015', 'descrip' => 'RUTAS', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'IOVAR016', 'descrip' => 'TRIBUNAL', 'estado' => 'AC', 'tipomov' => '2', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'HELIQ050', 'descrip' => 'H. SUPLEM. 50%', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
			['concodigo' => 'HELIQ100', 'descrip' => 'H. EXTRAS 100%', 'estado' => 'AC', 'tipomov' => '1', 'grupocodigo' => '00000001', 'empcodigo' => '00000001', ],
		]);
	}

}
