<?php

use Illuminate\Database\Seeder;

class CiudadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ciudades')->insert([
			['provcodigo' => '109', 'nombre' => 'GUAYAQUIL', 'ciucodigo' => '10901', ],
			['provcodigo' => '116', 'nombre' => 'RIOBAMBA', 'ciucodigo' => '11601', ],
			['provcodigo' => '110', 'nombre' => 'QUITO', 'ciucodigo' => '11001', ],
			['provcodigo' => '112', 'nombre' => 'CUENCA', 'ciucodigo' => '11201', ],
			['provcodigo' => '109', 'nombre' => 'SAMBORONDON', 'ciucodigo' => '10902', ],
			['provcodigo' => '122', 'nombre' => 'LOJA', 'ciucodigo' => '12201', ],
			['provcodigo' => '111', 'nombre' => 'POTOVIEJO', 'ciucodigo' => '11101', ],
		]);
    }
}
