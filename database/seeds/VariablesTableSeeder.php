<?php

use Illuminate\Database\Seeder;

class VariablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('variables')->insert([
	['idvariable' => '1', 'tipo_variable' => 'U', 'nombre' => 'Sistema Fecha Inicio', 'tipo_dato' => 'd', 'valor' => '2000-01-01', ],
	['idvariable' => '2', 'tipo_variable' => 'U', 'nombre' => 'Sistema Fecha Fin', 'tipo_dato' => 'd', 'valor' => '2010-12-31', ],
	['idvariable' => '3', 'tipo_variable' => 'S', 'nombre' => 'Sistema Fecha Minima', 'tipo_dato' => 'd', 'valor' => '1920-01-01', ],
	['idvariable' => '4', 'tipo_variable' => 'S', 'nombre' => 'Sistema Fecha Maxima', 'tipo_dato' => 'd', 'valor' => '2025-01-01', ],
	['idvariable' => '5', 'tipo_variable' => 'S', 'nombre' => 'Aplicacion Nombre', 'tipo_dato' => 'c', 'valor' => 'Nómina', ],
	['idvariable' => '6', 'tipo_variable' => 'U', 'nombre' => 'Recibos Nombre Empresa', 'tipo_dato' => 'b', 'valor' => '1', ],
	['idvariable' => '7', 'tipo_variable' => 'U', 'nombre' => 'Recibos Rol Copias', 'tipo_dato' => 'b', 'valor' => '0', ],
	['idvariable' => '8', 'tipo_variable' => 'U', 'nombre' => 'Anticipo Valor Maximo', 'tipo_dato' => 'n', 'valor' => '0.00', ],
	['idvariable' => '9', 'tipo_variable' => 'U', 'nombre' => 'Anticipo Pagos Numero', 'tipo_dato' => 'i', 'valor' => '0', ],
	['idvariable' => '10', 'tipo_variable' => 'U', 'nombre' => 'Nomina Pagos Numero', 'tipo_dato' => 'i', 'valor' => '2', ],
	['idvariable' => '11', 'tipo_variable' => 'U', 'nombre' => 'Parcial Primero Anticipo', 'tipo_dato' => 'n', 'valor' => '0.50', ],
	['idvariable' => '12', 'tipo_variable' => 'U', 'nombre' => 'Utilidad Persona Porcentaje', 'tipo_dato' => 'n', 'valor' => '10.00', ],
	['idvariable' => '13', 'tipo_variable' => 'U', 'nombre' => 'Utilidad Carga Porcentaje', 'tipo_dato' => 'n', 'valor' => '5.00', ],
	['idvariable' => '14', 'tipo_variable' => 'U', 'nombre' => 'Conexión a Sistema Externo', 'tipo_dato' => 'c', 'valor' => '', ],
	['idvariable' => '15', 'tipo_variable' => 'U', 'nombre' => 'Iess Descuento Parcial2', 'tipo_dato' => 'b', 'valor' => '0', ],
	['idvariable' => '16', 'tipo_variable' => 'U', 'nombre' => 'Nombre Sueldo Extra', 'tipo_dato' => 'c', 'valor' => 'GRATIF.EXTRA', ],
	['idvariable' => '17', 'tipo_variable' => 'S', 'nombre' => 'Seleccionar Impresora', 'tipo_dato' => 'b', 'valor' => '1', ],
	['idvariable' => '18', 'tipo_variable' => 'U', 'nombre' => 'Considerar Faltas en Calculo Iess', 'tipo_dato' => 'b', 'valor' => '1', ],
	['idvariable' => '19', 'tipo_variable' => 'U', 'nombre' => 'Permitir cambio de estado en Roles', 'tipo_dato' => 'b', 'valor' => '0', ],
	['idvariable' => '20', 'tipo_variable' => 'U', 'nombre' => 'Número Máximo de Horas Extras', 'tipo_dato' => 'n', 'valor' => '48', ],
	['idvariable' => '21', 'tipo_variable' => 'U', 'nombre' => 'Número Máximo de Horas Extras al 50%', 'tipo_dato' => 'n', 'valor' => '32', ],
	['idvariable' => '22', 'tipo_variable' => 'U', 'nombre' => 'Número Máximo de Horas Extras al 100%', 'tipo_dato' => 'n', 'valor' => '16', ],
	['idvariable' => '23', 'tipo_variable' => 'U', 'nombre' => 'Minimo de Horas a cumplir en Medio día de trabajo', 'tipo_dato' => 'c', 'valor' => '04:00', ],
	['idvariable' => '24', 'tipo_variable' => 'U', 'nombre' => 'Máximo de Horas a cumplir en día de trabajo', 'tipo_dato' => 'c', 'valor' => '08:00', ],
	['idvariable' => '25', 'tipo_variable' => 'U', 'nombre' => 'Base Imponible Impuesto Renta', 'tipo_dato' => 'n', 'valor' => '9720.00', ],
	['idvariable' => '26', 'tipo_variable' => 'U', 'nombre' => 'Valor minimo en rol por descuento de Imp. Rent.', 'tipo_dato' => 'n', 'valor' => '1.00', ],
	['idvariable' => '27', 'tipo_variable' => 'U', 'nombre' => 'Conexión a Sistema Contable', 'tipo_dato' => 'c', 'valor' => '', ],
	['idvariable' => '28', 'tipo_variable' => 'U', 'nombre' => 'Sombretiempo Fijo por Valor', 'tipo_dato' => 'b', 'valor' => '1', ],
	['idvariable' => '29', 'tipo_variable' => 'U', 'nombre' => 'Descontar del rol los dias de vacaciones', 'tipo_dato' => 'b', 'valor' => '1', ],
	['idvariable' => '30', 'tipo_variable' => 'U', 'nombre' => 'Descontar del rol los dias de falta', 'tipo_dato' => 'b', 'valor' => '0', ],
	['idvariable' => '31', 'tipo_variable' => 'U', 'nombre' => 'Presentar en rol los dias de Vacaciones', 'tipo_dato' => 'b', 'valor' => '0', ],
	['idvariable' => '32', 'tipo_variable' => 'U', 'nombre' => 'Manejo de Esquema de Docentes', 'tipo_dato' => 'b', 'valor' => '0', ],
	['idvariable' => '33', 'tipo_variable' => 'U', 'nombre' => 'Cantidad de Horas Laborables Mensual', 'tipo_dato' => 'n', 'valor' => '240', ],
	['idvariable' => '34', 'tipo_variable' => 'U', 'nombre' => 'Permitir que un empleado trabaje en diferentes compañias', 'tipo_dato' => 'b', 'valor' => '0', ],
	['idvariable' => '36', 'tipo_variable' => 'U', 'nombre' => 'Otorgar Sobregiro a los empleados', 'tipo_dato' => 'b', 'valor' => '1', ],
	['idvariable' => '37', 'tipo_variable' => 'U', 'nombre' => 'Porcentaje de pago minimo al empleado', 'tipo_dato' => 'n', 'valor' => '10.00', ],
	['idvariable' => '38', 'tipo_variable' => 'U', 'nombre' => 'Pagar proporcional de Fondo de Reserva en Decimo Tercer mes', 'tipo_dato' => 'b', 'valor' => '1', ],
	['idvariable' => '35', 'tipo_variable' => 'U', 'nombre' => 'Pagar Anticpo Completo a empleado nuevo', 'tipo_dato' => 'b', 'valor' => '1', ],
	['idvariable' => '39', 'tipo_variable' => 'U', 'nombre' => 'Número máximo de lineas en rol detallado', 'tipo_dato' => 'n', 'valor' => '10', ],
	['idvariable' => '40', 'tipo_variable' => 'U', 'nombre' => 'Calculo de Imp. Rta 0-Mensualizado', 'tipo_dato' => ' 1-Proyección ', 'valor' => 'n', ],
	['idvariable' => '41', 'tipo_variable' => 'U', 'nombre' => 'Factor para Empleados Discapacitados', 'tipo_dato' => 'n', 'valor' => '1', ],
]);
    }
}
