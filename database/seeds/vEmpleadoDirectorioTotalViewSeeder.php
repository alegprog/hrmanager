<?php

use Illuminate\Database\Seeder;

class vEmpleadoDirectorioTotalViewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("CREATE VIEW vempleado_directorio_total 
		AS 
		select DISTINCT e.grupocodigo,g.nombre grupo,e.empcodigo,e.razonsocial,e.establecimiento,m.emplecodigo,p.id_persona,
		       p.ci_tipo,p.ci,p.sexo,p.apellido1,p.apellido2,p.nombre1,p.nombre2,
		       CONCAT(LTRIM(RTRIM(p.apellido1)),' ', LTRIM(RTRIM(p.apellido2)),' ', LTRIM(RTRIM(p.nombre1)),' ', LTRIM(RTRIM(p.nombre2))) as completo,
		       p.fecha_nacimiento,p.id_iess, m.hassuple, m.hasextra, m.antiguedad,  
		       p.id_manejo,p.domicilio,p.telefono1,p.telefono2,
		       p.residencia, CASE p.residencia WHEN '01' THEN 'LOCAL' ELSE 'EXTERIOR' END AS nresidencia,
		       p.pais, pai.nombre AS npais,  
		       p.ref_familiar,p.ref_telefono,p.ref_domicilio,p.estado_civil,m.regiess, m.regmrl, 
		       m.sueldo,m.sueldo_ex,m.fecha_ingreso,m.fecha_sal,l.loccodigo,l.nombre localidad, l.horasextrasdet,
		       c.nombre cargo,d.nombre departamento,d.divcodigo, v.nombre AS division, v.centro, 
		       m.tipo_ingreso,d.deptocodigo,c.cargodptocodigo,m.tipopago,m.dctoiess,p.pic, CONCAT('C','',ltrim(rtrim(p.ci))) AS codigo_SAP, m.subjefe, m.directivo, 
		       m.todosben,m.todosdes,m.anticipo_rq,m.codigo_proveedor,m.estado,m.tipo, m.duracion, c.sectorial, s.descripcion as nsectorial, 
		       s.tarifa, m.horasextras, m.almuerzo, m.horcodigo, p.tipo_sangre, p.id_militar, m.marcacion,
		       p.discapacitado, p.tipo_discapacidad, p.familiar_discapacitado,  p.discapacidad, p.porcdisc, p.conadis, p.email,
		       m.pagofondo, m.pagodecimo13, m.pagodecimo14, m.afectado, 
		       p.tiene_asociacion, p.asociacion, p.condicion, m.rentapaga, m.tiporenta, m.iesspaga, m.locplanilla, i.nombre as nlocplanilla, 
		       r.region, CASE r.region WHEN '1' THEN 'Costa - Insular' ELSE 'Sierra - Oriente' END AS nregion,
		       b.tipopago AS formapago, CASE b.tipopago WHEN 'CUE' THEN 'P'
		                                     WHEN 'CHQ' THEN 'D' WHEN 'EFE' THEN 'D'    
		                                     ELSE 'A' END AS nformapago,
		       b.banco, coalesce(bn.nombre,'') AS nbanco,
		       CASE b.tipocuenta WHEN '03' THEN 'Corriente' 
		            WHEN '04' THEN 'Ahorro' ELSE '' END AS tipoCuenta,
		       b.cuenta                                                          
		FROM empleados m
		INNER JOIN personas p ON (p.id_persona = m.id_persona)
		INNER JOIN grupos g ON (g.grupocodigo = m.grupocodigo)
		INNER JOIN empresas e ON (e.grupocodigo = m.grupocodigo AND e.empcodigo = m.empcodigo)
		INNER JOIN localidades l ON (l.grupocodigo = m.grupocodigo AND l.empcodigo = m.empcodigo AND l.loccodigo = m.loccodigo)
		INNER JOIN departamentos d ON (d.grupocodigo = m.grupocodigo AND d.empcodigo = m.empcodigo AND d.deptocodigo = m.deptocodigo)
		INNER JOIN divisiones v ON (v.divcodigo = d.divcodigo)
		INNER JOIN cargos c ON (c.grupocodigo = m.grupocodigo AND c.empcodigo = m.empcodigo AND c.deptocodigo = m.deptocodigo AND c.cargodptocodigo = m.cargodptocodigo)
		LEFT JOIN cargo_sectoriales s ON (s.codigo = c.sectorial)
		INNER JOIN localidades i ON (i.grupocodigo = m.grupocodigo AND i.empcodigo = m.empcodigo AND i.loccodigo = m.locplanilla)
		INNER JOIN provincias r ON (r.provcodigo = l.provcodigo)
		INNER JOIN empleado_bancos b ON (b.grupocodigo = m.grupocodigo AND b.empcodigo = m.empcodigo AND b.emplecodigo = m.emplecodigo)
		INNER JOIN tipos pai ON (pai.tipo = 'PAI' AND pai.codigo = p.pais)
		LEFT JOIN tipos  bn ON (bn.tipo = 'BAN' AND bn.codigo = b.banco)");
    }
}
