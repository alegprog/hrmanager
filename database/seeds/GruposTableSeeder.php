<?php

use Illuminate\Database\Seeder;

class GruposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grupos')->insert([
			['grupocodigo' => '00000001', 'nombre' => 'MARRIOTT S.A.', ],
		]);
    }
}
