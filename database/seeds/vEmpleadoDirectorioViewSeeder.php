<?php

use Illuminate\Database\Seeder;

class vEmpleadoDirectorioViewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("CREATE view  vempleado_directorio as 
		select e.grupocodigo,g.nombre grupo,e.empcodigo,e.razonsocial,
		       m.emplecodigo,p.id_persona,p.ci_tipo,p.ci,m.tipopago,
		       p.apellido1,p.apellido2,p.nombre1,p.nombre2,
		       CONCAT(LTRIM(RTRIM(p.apellido1)),' ' ,LTRIM(RTRIM(p.apellido2)),' ',LTRIM(RTRIM(p.nombre1)),' ',LTRIM(RTRIM(p.nombre2))) AS completo,
		       d.nombre departamento,d.deptocodigo,m.codigo_proveedor,CONCAT('E',' ',LTRIM(RTRIM(p.ci))) as codigo_SAP,
		       l.nombre localidad, l.loccodigo, l.rotativo, 
		       m.cargodptocodigo, c.nombre cargo, m.subjefe,
		       m.estado,m.sueldo,m.sueldo_ex, m.hassuple, m.hasextra,m.directivo,  
		       m.fecha_ingreso,m.fecha_sal,m.regiess, m.regmrl, m.afectado,    
		       p.sexo,m.horasextras,m.marcacion,m.horcodigo, m.lider, m.antiguedad, 
		       m.pagofondo, m.almuerzo,m.rentapaga, m.tiporenta, m.iesspaga, m.locplanilla, i.nombre as nlocplanilla 
		from grupos g, empresas e, departamentos d, cargos c, personas p, empleados m, localidades l, localidades i
		where g.grupocodigo=e.grupocodigo
		and g.grupocodigo=d.grupocodigo and g.grupocodigo=c.grupocodigo
		and g.grupocodigo=m.grupocodigo 
		and e.empcodigo=d.empcodigo and e.empcodigo=c.empcodigo
		and e.empcodigo=m.empcodigo
		and d.deptocodigo=c.deptocodigo and d.deptocodigo=m.deptocodigo
		and l.empcodigo=e.empcodigo and l.grupocodigo=e.grupocodigo and l.loccodigo=m.loccodigo
		and c.cargodptocodigo=m.cargodptocodigo
		and p.id_persona=m.id_persona
		and m.grupocodigo = i.grupocodigo and m.empcodigo = i.empcodigo and m.locplanilla = i.loccodigo");
    }
}
