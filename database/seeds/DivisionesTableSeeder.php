<?php

use Illuminate\Database\Seeder;

class DivisionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('divisiones')->insert([
			['divcodigo' => 'DIV01', 'principal' => 'ECO. GUSTAVO MARRIOTT PEREZ', 'nombre' => 'PRESIDENCIA', 'centro' => '', ],
			['divcodigo' => 'DIV02', 'principal' => '', 'nombre' => 'GUAYAQUIL', 'centro' => '', ],
			['divcodigo' => 'DIV03', 'principal' => '', 'nombre' => 'QUITO', 'centro' => '', ],
		]);
    }
}
