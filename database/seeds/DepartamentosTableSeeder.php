<?php

use Illuminate\Database\Seeder;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departamentos')->insert([
	['deptocodigo' => 'DPT00001', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'PRESIDENCIA', 'divcodigo' => 'DIV01', 'principal' => 'ECO. GUSTAVO MARRIOTT PEREZ', 'codigo_proveedor' => '', 'centro' => '3.11', ],
	['deptocodigo' => 'DPT00002', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'VENTAS COMERCIAL GYE', 'divcodigo' => 'DIV02', 'principal' => 'ING. ROBERTO MUJICA SALAME', 'codigo_proveedor' => '', 'centro' => '2.6', ],
	['deptocodigo' => 'DPT00003', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'VENTAS EMPRESARIAL Y CORPORATIVOS', 'divcodigo' => 'DIV02', 'principal' => 'ING. LORENA VALLADARES SALGADO', 'codigo_proveedor' => '', 'centro' => '2.5', ],
	['deptocodigo' => 'DPT00004', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'MERCADEO', 'divcodigo' => 'DIV02', 'principal' => 'TEC. RAFAEL AVELLAN', 'codigo_proveedor' => '', 'centro' => '3.6', ],
	['deptocodigo' => 'DPT00005', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'RECURSOS HUMANOS', 'divcodigo' => 'DIV02', 'principal' => 'DRA. ANGELA MARRIOTT PEREZ', 'codigo_proveedor' => '', 'centro' => '3.3', ],
	['deptocodigo' => 'DPT00006', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'COMPUTACION', 'divcodigo' => 'DIV02', 'principal' => 'LCDA. CARMEN VELASCO VALERO', 'codigo_proveedor' => '', 'centro' => '3.5', ],
	['deptocodigo' => 'DPT00007', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'REVISORIA', 'divcodigo' => 'DIV02', 'principal' => 'ECON. FABRICIO ESTRELLA GOMEZ', 'codigo_proveedor' => '', 'centro' => '3.4', ],
	['deptocodigo' => 'DPT00008', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'CONTABILIDAD', 'divcodigo' => 'DIV02', 'principal' => 'ECON. ENRIQUE PASTORIZA ALCIVAR', 'codigo_proveedor' => '', 'centro' => '3.2', ],
	['deptocodigo' => 'DPT00009', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'COBRANZAS', 'divcodigo' => 'DIV02', 'principal' => 'ECON. FABRICIO GUERRERO MORALES', 'codigo_proveedor' => '', 'centro' => '3.2', ],
	['deptocodigo' => 'DPT00010', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'ALMACEN QUITO', 'divcodigo' => 'DIV03', 'principal' => 'IVAN MARTINEZ DE LA TORRE', 'codigo_proveedor' => '', 'centro' => '1.1', ],
	['deptocodigo' => 'DPT00011', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'VENTAS COMERCIAL UIO', 'divcodigo' => 'DIV03', 'principal' => 'ING. JIMMY ITURRALDE VASCO', 'codigo_proveedor' => '', 'centro' => '1.3', ],
	['deptocodigo' => 'DPT00012', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'VENTAS INDUSTRIAL UIO', 'divcodigo' => 'DIV03', 'principal' => 'ING. ISBELA MACIAS DUARTE', 'codigo_proveedor' => '', 'centro' => '1.2', ],
	['deptocodigo' => 'DPT00013', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'GERENCIA GENERAL', 'divcodigo' => 'DIV01', 'principal' => 'ING. EDUARDO VITERI MARRIOTT', 'codigo_proveedor' => '', 'centro' => '2.9', ],
	['deptocodigo' => 'DPT00014', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'GERENCIA ALMACEN', 'divcodigo' => 'DIV01', 'principal' => 'SRA. PIEDAD MARRIOTT PEREZ', 'codigo_proveedor' => '', 'centro' => '3.1', ],
	['deptocodigo' => 'DPT00015', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'BODEGA CENTRAL', 'divcodigo' => 'DIV02', 'principal' => 'LUIS MARIDUNIA JAYA', 'codigo_proveedor' => '', 'centro' => '3.7', ],
	['deptocodigo' => 'DPT00016', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'FABRICA', 'divcodigo' => 'DIV02', 'principal' => 'ING. JOSE YAGUAL PAZMINIO', 'codigo_proveedor' => '', 'centro' => '3.10', ],
	['deptocodigo' => 'DPT00017', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'ALMACEN LUQUE', 'divcodigo' => 'DIV02', 'principal' => 'GERENTE ALMACEN', 'codigo_proveedor' => '', 'centro' => '2.1', ],
	['deptocodigo' => 'DPT00018', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'ALMACEN MEGASTORE', 'divcodigo' => 'DIV02', 'principal' => '', 'codigo_proveedor' => '', 'centro' => '2.2', ],
	['deptocodigo' => 'DPT00019', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'ALMACEN DICENTRO', 'divcodigo' => 'DIV02', 'principal' => '', 'codigo_proveedor' => '', 'centro' => '2.4', ],
	['deptocodigo' => 'DPT00020', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'nombre' => 'VENTAS INDUSTRIAL GUAYAQUIL', 'divcodigo' => 'DIV02', 'principal' => 'ING. FRANCISCO MUENTES', 'codigo_proveedor' => '', 'centro' => '2.5', ],
]);
    }
}
