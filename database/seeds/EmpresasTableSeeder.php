<?php

use Illuminate\Database\Seeder;

class EmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('empresas')->insert([
		['empcodigo' => '00000001', 'grupocodigo' => '00000001', 'razonsocial' => '01 MARRIOTT S.A.', 'ruc' => '0990247595001', 'localidad' => 'VIA SAMBORONDON KM 1.5', 'reprenlegal' => 'ECO.GUSTAVO MARRIOTT PEREZ', 'patronal' => '12076394', 'pais' => 'SAMBORONDON-ECUADO', 'rucContador' => '0917013633001', 'contador' => 'Pastoriza Alcivar Enrique Daniel', 'sector' => 'PUB', 'email' => 'nomina@marriott.ec', 'logotipo' => 'C:\HRMANAGER\NOMINA\IMAGE\LOGO.BMP', 'establecimiento' => '001', ],
		]);
    }
}
