<?php

use Illuminate\Database\Seeder;

class LocalidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('localidades')->insert([
	['loccodigo' => 'LOC00001', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'tipo' => 'P', 'nombre' => 'OF.GUAYAQUIL', 'domicilio' => 'LUQUE 323 Y CHIMBORAZO', 'principal' => 'ECO. GUSTAVO MARRIOTT PEREZ', 'parroquia' => 'ROCA', 'ciucodigo' => '10901', 'provcodigo' => '109', 'telefono1' => '042327888', 'telefono2' => '042322951', 'id_biometrico' => '1', 'modelobio' => '', 'biometrico' => '1', 'dia_corte' => '20', 'almuerzo' => '1', 'horasextrasdet' => '0', 'rotativo' => '0', 'punto' => '001', ],
	['loccodigo' => 'LOC00002', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'tipo' => 'S', 'nombre' => 'MEGASTORE', 'domicilio' => 'VIA A SAMBORONDON KM. 1.5 URBANIZACION EL PORTON PRINCIPAL S/N', 'principal' => 'ECO. GUSTAVO MARRIOTT PEREZ', 'parroquia' => 'SAMBORONDON', 'ciucodigo' => '10902', 'provcodigo' => '109', 'telefono1' => '042830423', 'telefono2' => '042831737', 'id_biometrico' => '4', 'modelobio' => '', 'biometrico' => '1', 'dia_corte' => '20', 'almuerzo' => '0', 'horasextrasdet' => '0', 'rotativo' => '1', 'punto' => '001', ],
	['loccodigo' => 'LOC00003', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'tipo' => 'S', 'nombre' => 'ALM LUQUE', 'domicilio' => 'LUQUE 323 Y CHIMBORAZO', 'principal' => 'ECO. GUSTAVO MARRIOTT PEREZ', 'parroquia' => 'ROCA', 'ciucodigo' => '10901', 'provcodigo' => '109', 'telefono1' => '042327888', 'telefono2' => '', 'id_biometrico' => '5', 'modelobio' => '', 'biometrico' => '1', 'dia_corte' => '20', 'almuerzo' => '1', 'horasextrasdet' => '0', 'rotativo' => '0', 'punto' => '001', ],
	['loccodigo' => 'LOC00004', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'tipo' => 'S', 'nombre' => 'DICENTRO', 'domicilio' => 'CC DICENTRO', 'principal' => 'ING. EDUARDO VITERI MARRIOTT', 'parroquia' => 'TARQUI', 'ciucodigo' => '10901', 'provcodigo' => '109', 'telefono1' => '2241041', 'telefono2' => '2241045', 'id_biometrico' => '6', 'modelobio' => '', 'biometrico' => '1', 'dia_corte' => '20', 'almuerzo' => '1', 'horasextrasdet' => '0', 'rotativo' => '1', 'punto' => '001', ],
	['loccodigo' => 'LOC00005', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'tipo' => 'S', 'nombre' => 'SALITRE', 'domicilio' => 'VIA A SAMBORONDON KM. 1.5 URBANIZACION EL PORTON PRINCIPAL S/N', 'principal' => 'ECO. GUSTAVO MARRIOTT PEREZ', 'parroquia' => 'SAMBORONDON', 'ciucodigo' => '10902', 'provcodigo' => '109', 'telefono1' => '042830710', 'telefono2' => '', 'id_biometrico' => '3', 'modelobio' => '', 'biometrico' => '1', 'dia_corte' => '20', 'almuerzo' => '0', 'horasextrasdet' => '1', 'rotativo' => '0', 'punto' => '001', ],
	['loccodigo' => 'LOC00006', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'tipo' => 'S', 'nombre' => 'FABRICA', 'domicilio' => 'VIA A SAMBORONDON KM. 1.5 URBANIZACION EL PORTON PRINCIPAL S/N', 'principal' => 'ECO. GUSTAVO MARRIOTT PEREZ', 'parroquia' => 'SAMBORONDON', 'ciucodigo' => '10902', 'provcodigo' => '109', 'telefono1' => '042830710', 'telefono2' => '', 'id_biometrico' => '7', 'modelobio' => '', 'biometrico' => '1', 'dia_corte' => '20', 'almuerzo' => '0', 'horasextrasdet' => '1', 'rotativo' => '0', 'punto' => '001', ],
	['loccodigo' => 'LOC00007', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'tipo' => 'S', 'nombre' => 'ALM QUITO', 'domicilio' => 'AV. AMERICA 5167 Y VILLALENGUA', 'principal' => 'ECO. GUSTAVO MARRIOTT PEREZ', 'parroquia' => '', 'ciucodigo' => '11001', 'provcodigo' => '110', 'telefono1' => '022243352', 'telefono2' => '022243345', 'id_biometrico' => '2', 'modelobio' => '', 'biometrico' => '1', 'dia_corte' => '20', 'almuerzo' => '1', 'horasextrasdet' => '0', 'rotativo' => '1', 'punto' => '001', ],
	['loccodigo' => 'LOC00008', 'empcodigo' => '00000001', 'grupocodigo' => '00000001', 'tipo' => 'S', 'nombre' => 'OF. QUITO', 'domicilio' => 'AV. AMERICA 5167 Y VILLALENGUA', 'principal' => 'ECO. GUSTAVO MARRIOTT PEREZ', 'parroquia' => '', 'ciucodigo' => '11001', 'provcodigo' => '110', 'telefono1' => '022243316', 'telefono2' => '022455339', 'id_biometrico' => '0', 'modelobio' => '', 'biometrico' => '1', 'dia_corte' => '20', 'almuerzo' => '1', 'horasextrasdet' => '0', 'rotativo' => '0', 'punto' => '001', ],
]);
    }
}
