<?php

use Illuminate\Database\Seeder;

class EmpresaVariablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('empresa_variables')->insert([
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '1', 'valor' => '2010-12-01', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '2', 'valor' => '2025-12-31', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '6', 'valor' => '1', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '7', 'valor' => '1', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '8', 'valor' => '0.00', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '9', 'valor' => '1', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '10', 'valor' => '2', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '11', 'valor' => '0.30', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '12', 'valor' => '10.00', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '13', 'valor' => '5.00', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '14', 'valor' => 'Driver={SQL Server};Server=192.168.100.11\SQL2008;Database=NitgenAccessManager;uid=sa;pwd=MARRIOTT;', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '15', 'valor' => '0', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '16', 'valor' => 'SOBRESUELDO', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '18', 'valor' => '1', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '19', 'valor' => '0', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '20', 'valor' => '48', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '21', 'valor' => '32', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '22', 'valor' => '16', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '23', 'valor' => '05:00', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '24', 'valor' => '08:00', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '25', 'valor' => '9720.00', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '26', 'valor' => '1.00', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '27', 'valor' => '', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '28', 'valor' => '1', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '29', 'valor' => '1', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '30', 'valor' => '0', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '31', 'valor' => '0', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '32', 'valor' => '0', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '33', 'valor' => '240.00', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '34', 'valor' => '0', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '36', 'valor' => '0', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '37', 'valor' => '0.00', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '38', 'valor' => '1', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '35', 'valor' => '0', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '39', 'valor' => '20', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '40', 'valor' => '1.00', ],
	['grupocodigo' => '00000001', 'empcodigo' => '00000001', 'idvariable' => '41', 'valor' => '30', ],
]);
    }
}
