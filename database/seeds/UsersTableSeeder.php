<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'codigo' => 'ADMIN',
            'nombre' => 'CARMEN VELASCO',
            'password' => bcrypt('123456'),
            'fecha_ingreso' => '2010/12/07',
            'estado' => 'AC',
            'tipo' => 'ADM',
        ]);

        DB::table('users')->insert([
            'codigo' => 'AMARRIOT',
            'nombre' => 'ANGELA MARRIOTT',
            'password' => bcrypt('123456'),
            'fecha_ingreso' => '2010/12/07',
            'estado' => 'IN',
            'tipo' => 'RRH',
        ]);
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
            'codigo' => 'DEMO',
            'nombre' => 'DEMOSTRACION',
            'password' => bcrypt('123456'),
            'fecha_ingreso' => '2010/08/01',
            'estado' => '',
            'tipo' => 'CON',
        ]);
        
		DB::table('users')->insert([
            'codigo' => 'ENRIQUE',
            'nombre' => 'ENRIQUE PASTORIZA',
            'password' => bcrypt('123456'),
            'fecha_ingreso' => '2012/02/03',
            'estado' => 'AC',
            'tipo' => 'CON',
        ]);

        DB::table('users')->insert([
            'codigo' => 'KNAVARRO',
            'nombre' => 'KAREN NAVARRO',
            'password' => bcrypt('123456'),
            'fecha_ingreso' => '2010/12/07',
            'estado' => 'AC',
            'tipo' => 'RRH',
        ]);

        DB::table('users')->insert([
            'codigo' => 'OMAR',
            'nombre' => 'OMAR NOBOA',
            'password' => bcrypt('123456'),
            'fecha_ingreso' => '2010/08/01',
            'estado' => 'AC',
            'tipo' => 'ADM',
        ]);


        DB::table('users')->insert([
            'codigo' => 'PMORA',
            'nombre' => 'PAMELA MORA',
            'password' => bcrypt('123456'),
            'fecha_ingreso' => '2014/03/01',
            'estado' => 'AC',
            'tipo' => 'RRH',
        ]);


        DB::table('users')->insert([
            'codigo' => 'RCLARK',
            'nombre' => 'ROSITA CLARK',
            'password' => bcrypt('123456'),
            'fecha_ingreso' => '2010/12/07',
            'estado' => 'AC',
            'tipo' => 'RRH',
        ]);


        DB::table('users')->insert([
            'codigo' => 'VBASANTE',
            'nombre' => 'MA. VERONICA BASANTES',
            'password' => bcrypt('123456'),
            'fecha_ingreso' => '2010/12/07',
            'estado' => 'AC',
            'tipo' => 'RRH',
        ]);
    }
}
