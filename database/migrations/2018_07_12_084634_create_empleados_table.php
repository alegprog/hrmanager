<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->string('emplecodigo',8);
            $table->string('empcodigo',8);
            $table->string('grupocodigo',8);
            $table->string('tipo',2);
            $table->string('duracion',2);
            $table->string('deptocodigo',8);
            $table->string('estado',2);
            $table->double('sueldo',12,2);
            $table->string('cargodptocodigo',8);
            $table->string('tipo_ingreso',1);
            $table->string('loccodigo',8);
            $table->string('scalacodigo',6);
            $table->date('fecha_ingreso')->nullable();
            $table->date('fecha_registro')->nullable();
            $table->string('tipopago',2);
            $table->tinyInteger('dctoiess')->default(1);
            $table->date('fecha_sal')->nullable();
            $table->tinyInteger('todosben')->default(1);
            $table->tinyInteger('todosdes')->default(1);
            $table->double('sueldo_ex',12,2)->default(0);
            $table->tinyInteger('anticipo')->default(0);
            $table->double('anticipo_rq',12,2);
            $table->string('codigo_proveedor',10);
            $table->integer('id_persona');
            $table->tinyInteger('horasextras')->default(0);
            $table->double('hassuple',8,2)->default(0);
            $table->double('hasextra',8,2)->default(0);
            $table->tinyInteger('antiguedad')->default(0);
            $table->tinyInteger('marcacion')->default(0);
            $table->string('horaingreso',8)->default('00:00');
            $table->string('horasalida',8)->default('00:00');
            $table->string('tiempoalmuerzo',8)->default('00:00');
            $table->string('hastrabajo',8)->default('00:00');
            $table->tinyInteger('pagofondo')->default(0);
            $table->tinyInteger('lider')->default(0);
            $table->string('horcodigo',4);
            $table->tinyInteger('almuerzo')->default(0);
            $table->tinyInteger('subjefe')->default(0);
            $table->string('centro',8);
            $table->string('rentapaga',1);
            $table->string('tiporenta',1)->default('M');
            $table->string('iesspaga',1);
            $table->string('locplanilla',8);
            $table->tinyInteger('extsiess')->default(0);
            $table->tinyInteger('directivo')->default(0);
            $table->tinyInteger('regiess')->default(0);
            $table->tinyInteger('regmrl')->default(0);
            $table->double('hasnoctu',8,2)->default(0);
            $table->tinyInteger('pagodecimo13')->default(0);
            $table->tinyInteger('pagodecimo14')->default(0);
            $table->string('fecha_grupo',19);
            $table->string('entidadacumula',3);
            $table->tinyInteger('docente')->default(0);
            $table->tinyInteger('vendedor')->default(0);
            $table->tinyInteger('afectado')->default(0);
            $table->string('fechagrupo',19);
            $table->tinyInteger('comisionista')->default(0);
            $table->string('paging',1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
