<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->string('rolcodigo',8);
            $table->string('empcodigo',8);
            $table->string('grupocodigo',8);
            $table->string('ano',4);
            $table->string('mes',2);
            $table->string('estado',2);
            $table->string('fecha_registro',19);
            $table->string('fecha_pago',19);
            $table->string('tipo',2);
            $table->tinyInteger('parcial')->default(1);
            $table->tinyInteger('aprobado')->default(0);
            $table->string('fecha_aprobacion',19);
            $table->string('usuario_aprobacion',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
