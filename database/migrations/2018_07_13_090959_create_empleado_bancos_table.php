<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadoBancosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleado_bancos', function (Blueprint $table) {
            $table->string('emplecodigo',8);
            $table->string('empcodigo',8);
            $table->string('grupocodigo',8);
            $table->string('cuenta',20);
            $table->string('banco',100);
            $table->string('tipocuenta',3);
            $table->string('tipopago',3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleado_bancos');
    }
}
