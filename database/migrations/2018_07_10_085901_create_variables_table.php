<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variables', function (Blueprint $table) {
            $table->integer('idvariable');
            $table->enum('tipo_variable',['S','U']); 
            $table->string('nombre',64);
            $table->string('tipo_dato',64);            
            //$table->enum('tipo_dato',['b','c','d','n','i']);
            $table->string('valor',64);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variables');
    }
}
