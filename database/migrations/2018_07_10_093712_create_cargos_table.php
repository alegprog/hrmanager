<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargos', function (Blueprint $table) {
            $table->string('deptocodigo',8);
            $table->string('empcodigo',8);
            $table->string('grupocodigo',8);
            $table->string('cargodptocodigo',8);
            $table->string('tiposueldo',1);
            $table->string('nombre',200);
            $table->double('sueldo',12,2);
            $table->string('estado',2);
            $table->string('adpto',1);
            $table->string('sectorial',13)->default('0000000000000');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargos');
    }
}
