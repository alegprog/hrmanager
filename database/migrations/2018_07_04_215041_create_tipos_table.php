<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos', function (Blueprint $table) {
            $table->string('tipo',3);
            $table->string('codigo',3);
            $table->string('nombre',100);
            $table->double('valor',12,2);
            $table->double('porcentaje',12,2);
            $table->string('estado',2);
            $table->string('codrel',8);
            $table->string('cuenta',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos');
    }
}
