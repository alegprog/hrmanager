<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo',20);
            $table->string('nombre',50);
            $table->string('password',200);
            $table->date('fecha_ingreso');
            $table->date('fecha_salida')->nullable();
            $table->string('estado',2)->default('AC');
            $table->string('tipo',3);
            $table->tinyInteger('enviaremail')->default(0);
            $table->string('perfil',200)->nullable();
            $table->string('email',200)->nullable();
            $table->string('ekey',200)->nullable();
            /*$table->string('name');
            $table->string('email')->unique();
            $table->string('password');*/
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
