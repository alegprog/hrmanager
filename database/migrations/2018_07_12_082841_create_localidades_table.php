<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localidades', function (Blueprint $table) {
            $table->string('loccodigo',8);
            $table->string('empcodigo',8);
            $table->string('grupocodigo',8);
            $table->string('tipo',3);
            $table->string('nombre',60);
            $table->string('domicilio',255);
            $table->string('principal',255);
            $table->string('parroquia',60);
            $table->string('ciucodigo',5);
            $table->string('provcodigo',5);
            $table->string('telefono1',10);
            $table->string('telefono2',10);
            $table->integer('id_biometrico')->default(0);;
            $table->string('modelobio',30);
            $table->tinyInteger('biometrico')->default(0);
            $table->tinyInteger('dia_corte')->default(27);
            $table->tinyInteger('almuerzo')->default(0);
            $table->tinyInteger('horasextrasdet')->default(0);
            $table->tinyInteger('rotativo')->default(0);
            $table->string('punto',3)->default('001');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('localidades');
    }
}
