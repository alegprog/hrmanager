<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departamentos', function (Blueprint $table) {
            $table->string('deptocodigo',8);
            $table->string('empcodigo',8);
            $table->string('grupocodigo',8);
            $table->string('nombre',60);
            $table->string('divcodigo',8);
            $table->string('principal',60);    
            $table->string('codigo_proveedor',10);
            $table->string('centro',8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departamentos');
    }
}
