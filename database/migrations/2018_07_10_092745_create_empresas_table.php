<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->string('empcodigo',8);
            $table->string('grupocodigo',8);
            $table->string('razonsocial',60);
            $table->string('ruc',15);
            $table->string('localidad',60);
            $table->string('reprenlegal',60);
            $table->string('patronal',10);
            $table->string('pais',18);
            $table->string('rucContador',13);
            $table->string('contador',50);
            $table->string('sector',3);
            $table->string('email',200);
            $table->string('logotipo',250);
            $table->string('establecimiento',3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
