<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargoSectorialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargo_sectoriales', function (Blueprint $table) {            
            $table->integer('comision');
            $table->string('codigo',13);
            $table->string('descripcion',200);
            $table->string('tarifa',200); //$table->double('tarifa',12,2);
            $table->string('estado',200)->default('AC');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargo_sectoriales');
    }
}
