<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->integer('id_persona');
            $table->string('ci',15);
            $table->string('ci_tipo',1);
            $table->string('nombre1',20);
            $table->string('nombre2',20);
            $table->string('apellido1',20);
            $table->string('apellido2',20);
            $table->string('sexo',1);
            $table->string('estado_civil',1);
            $table->string('fecha_nacimiento',10);
            $table->string('id_iess',12);
            $table->string('licencia',15)->default('0');
            $table->string('id_manejo',12);
            $table->string('domicilio',250);
            $table->string('telefono1',14);
            $table->string('telefono2',14);
            $table->string('ref_familiar',80);
            $table->string('ref_telefono',14);
            $table->string('ref_domicilio',250);
            $table->string('fecha_registro',16);
            $table->string('fecha_modificacion',16);
            $table->string('pic',250);
            $table->string('estado',2);
            $table->string('id_militar',12);
            $table->string('provincia_nacimiento',3);
            $table->string('ciudad_nacimiento',5);
            $table->string('dispensario_IESS',30);
            $table->string('tipo_sangre',3);
            $table->string('categoria_licencia',3);
            $table->string('licencia_caduca',10);
            $table->string('tiene_asociacion',1)->default('N');
            $table->string('asociacion',50);
            $table->string('condicion',250);
            $table->string('tipo_discapacidad',1);
            $table->string('discapacidad',50);
            $table->double('porcdisc',5,2)->default('0');
            $table->string('conadis',10);               
            $table->tinyInteger('vivienda')->default('0');
            $table->double('alquiler',12,2)->default('0');
            $table->tinyInteger('hipoteca')->default('0');
            $table->double('valor_hipoteca',12,2)->default('0');
            $table->string('entidad_hipoteca',3);
            $table->tinyInteger('no_personas')->default('0');
            $table->string('email',250);
            $table->string('pais',0)->default('000');
            $table->string('residencia',2)->default('01');
            $table->string('familiar_discapacitado',10);
            $table->string('discapacitado',2);
            $table->tinyInteger('aprobado')->default('0');
            $table->string('fecha_aprobacion',16);
            $table->string('usuario_aprobacion',10); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
