<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@authenticate')->name('login.post');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');
$this->get('getlogout', 'Auth\LoginController@getLogout')->name('getlogout');

Route::get('/', function () {
	return redirect()->route('login');
});

Route::middleware(['auth'])->prefix('dashboard')->group(function () {

	Route::get('/', 'DashboardController@index')->name('dashboard.index');

	Route::get('personas', 'PersonController@index')->name('person.index');	
	Route::get('persona/detalle/{id}', 'PersonController@show')->name('person.show');
	Route::get('personas/data', 'PersonController@data')->name('person.data');


	Route::post('/', 'DashboardController@search')->name('search');
});
