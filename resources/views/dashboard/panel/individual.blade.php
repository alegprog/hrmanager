@if(count(auth()->user()->institutions)>0)
 Bienvenido a la Institucion:<br>
 @php 
   $institution = auth()->user()->institutions->first();
   //dd($institution);
 @endphp
 <b>{{$institution->name}}</b><br>
 <img src="{{ $institution->logo ? asset('file/'.$institution->logo) : asset('assets/template/light/img/default-thumbnail.jpg') }}" alt="profile pic holder" style="width: 80px;" class="img-thumbnail" > &nbsp; 
 <a class="btn btn-primary" href="{{route('institution.editIndividual')}}">Editar</a>
@else
  <div class="alert alert-warning"> - No posee instituciones asociada.</div>
@endif