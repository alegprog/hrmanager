<div class="row">
{!!Form::open(['route' => 'search']) !!}
<div class="col-sm-6 col-md-3">
 <div class="row">
  <div class="form-group">
          <label class="col-sm-12 control-label" for="example-select">Grupo</label>
          <div class="col-sm-12">
            {!!Form::select('grupo', array_pluck($grupos,'nombre','grupocodigo'), $grupo , ['class'=>'form-control'])!!}
            @if ($errors->has('grupo'))
                <span class="help-block">
                    <strong>{{ $errors->first('grupo') }}</strong>
                </span>
            @endif
          </div>
      </div>
  </div>
</div>
<div class="col-sm-6 col-md-4">
 <div class="row">
  <div class="form-group">
      <label class="col-sm-12 control-label" for="example-select">Empresa</label>
      <div class="col-sm-12">
        {!!Form::select('empresa', array_pluck($empresas,'razonsocial','empcodigo'), $empresa , ['class'=>'form-control'])!!}
        @if ($errors->has('empresa'))
            <span class="help-block">
                <strong>{{ $errors->first('empresa') }}</strong>
            </span>
        @endif
      </div>
   </div>
  </div>
</div>
<div class="col-sm-6 col-md-3">
 <div class="row">
  <div class="form-group">
          <label class="col-sm-12 control-label" for="example-select">Rol</label>
          <div class="col-sm-12">
              {!!Form::select('rol', array_pluck($roles,'rolcodigo','rolcodigo'), $rol , ['class'=>'form-control'])!!}
              @if ($errors->has('empresa'))
                  <span class="help-block">
                      <strong>{{ $errors->first('empresa') }}</strong>
                  </span>
              @endif
          </div>
      </div>
  </div>
</div>
<div class="col-sm-6 col-md-2">
 
  <div class="form-group">
    <label class="col-sm-12 control-label" for="example-select">&nbsp;</label>
    <button type="submit" class="btn btn-primary">Filtrar</button>
  </div>
  
</div>
{!!Form::close() !!}
<div class="col-xs-12"><hr></div>
<div class="col-sm-6 col-md-6">
    <div class="panel panel-default">        
        <div class="panel-body">
            <div class="text-left">
              <i class="fa-2x padding-top-small padding-bottom padding-right-small fa fa-users pull-left text-primary"></i>
              <h5 style="padding-top: 5px;" class="text-primary"><b >Colaboradores Activo</b></h5>
              <hr>
            </div>
            <div class="row">

            <div class="col-xs-12 col-sm-3 text-center">
              <h5 class="text-dark"><b id="widget_count1">{{$total_empleado}}</b></h5>
              <p>Total</p>
            </div>
            <div class="col-xs-12  col-sm-3 text-center">
              <h5 class="text-dark"><b id="widget_count1">{{$total_hombres}}</b></h5>
              <p>Hombre</p>
            </div>
            <div class="col-xs-12 col-sm-3 text-center">
              <h5 class="text-dark"><b id="widget_count1">{{$total_mujeres}}</b></h5>
              <p>Mujeres</p>
            </div>
            <div class="col-xs-12 col-sm-3 text-center">
              <h5 class="text-dark"><b id="widget_count1">{{$total_sin_discapacidad}}</b></h5>
              <p>Sin Discapacidad</p>
            </div>
            <div class="col-xs-12" style="margin-bottom: 5px;"></div>
            <div class="clearfix" style="border-bottom: 1px solid #ccc;margin-bottom: 15px"></div>
              
            <div class="col-xs-12 col-sm-4 text-center">
              <h5 class="text-dark"><b id="widget_count1">{{$total_con_discapacidad+$total_tutores_discapacidad}}</b></h5>
              <p>Con Discapacidad</p>
            </div>

            <div class="col-xs-12 col-sm-4 text-center">
              <h5 class="text-dark"><b id="widget_count1">{{$total_tutores_discapacidad}}</b></h5>
              <p>Tutores Discapacitado</p>
            </div>           

            <div class="col-xs-12 col-sm-4 text-center">
              <h5 class="text-dark"><b id="widget_count1">{{round($total_brecha_discapacidad)}}</b></h5>
              <p>Brecha Discapacitado</p>
            </div>
            

            <div class="col-xs-12" style="margin-bottom: 5px;"></div>
            {{--<div class="clearfix" style="border-bottom: 1px solid #ccc;margin-bottom: 10px;"></div>--}}
            
            

            </div>
        </div>
    </div>
</div>

<div class="col-sm-3 col-md-3">
    <div class="panel panel-default">        
        <div class="panel-body">
            <div class="text-left">
              <i class="fa-2x padding-top-small padding-bottom padding-right-small fa fa-users pull-left text-success"></i>
              <h5 style="padding-top: 0px;" class="text-success"><b >Colaboradores Entrante</b></h5>
              <hr>
            </div>
            <div class="col-xs-12 text-right">
              <h5 class="text-dark"><b id="widget_count1">{{$total_empleado_entrante}}</b></h5>
              <p>Total</p>
            </div>            
           
        </div>
    </div>
</div>


<div class="col-sm-3 col-md-3">
    <div class="panel panel-default">        
        <div class="panel-body">
            <div class="text-left">
              <i class="fa-2x padding-top-small padding-bottom padding-right-small fa fa-users pull-left text-danger"></i>
              <h5 style="padding-top: 0px;" class="text-danger"><b >Colaboradores Salientes</b></h5>
              <hr>
            </div>
            <div class="col-xs-12 text-right">
              <h5 class="text-dark"><b id="widget_count1">{{$total_empleado_saliente}}</b></h5>
              <p>Total</p>
            </div>            
           
        </div>
    </div>
</div>
</div>
