@extends('layouts.master')

@section('breadcrumb')
<!-- Content Header (Page header) -->
<section class="content-header">
    <!--section starts-->
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="javascript:void(0);">
                <i class="fa fa-fw ti-home"></i> Dashboard
            </a>
        </li>       
        
    </ol>
</section>
@endsection

@section('content')
  <div class="row">
   @include('dashboard.panel.general')

   {{--@if(auth()->user()->type=='manager')

	   @include('dashboard.panel.manager')

   @elseif(auth()->user()->type=='multiple')

     @include('dashboard.panel.multiple')

   @elseif(auth()->user()->type=='individual')

     @include('dashboard.panel.individual')

   @endif--}}

   </div>

@endsection