@extends('layouts.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/template/light/vendors/datatables/css/dataTables.bootstrap.css')}}"/>
@endsection



@section('breadcrumb')
<!-- Content Header (Page header) -->
<section class="content-header">
    <!--section starts-->
    <h1>
        Listado de Personas
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard.index')}}">
                <i class="fa fa-fw ti-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{route('person.index')}}">Personas</a>
        </li>
        <li class="active">
            <a href="{{route('person.index')}}">
                Listado
            </a>
        </li>
    </ol>
</section>
@endsection

@section('content')
    
    <div class="clearfix"></div>
    <div class="row" style="margin-top:15px;">
    <div class="col-lg-12">
        <div class="panel ">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-fw fa-angle-double-right"></i> Personas
                </h3>
                <span class="pull-right hidden-xs">
                {{--<i class="fa fa-fw ti-angle-up clickable"></i>
                <i class="fa fa-fw ti-close removepanel clickable"></i>--}}
            </span>
            </div>
            <div class="panel-body">
                <table id="listTable" class="table horizontal_table table-bordered table-striped" >
                    <thead>
                        <tr>
                            <th class="" width="20"></th>
                            <th class="">Tipo Documento</th>
                            <th class="">Identificación</th>
                            <th class="" width="150">Primer Nombre</th>
                            <th class="" width="150">Segundo Nombre</th>
                            <th class="" width="150">Primer Apellido</th>
                            <th class="" width="150">Segundo Apellido</th>
                            <th class="">Sexo</th>
                            <th class="" width="100" >Estado Civil</th>
                            <th class="">Fecha de Nacimiento</th>
                            <th class="" style="width: 700px !important;">Domicilio</th>
                            <th class="" width="150">Teléfono 1</th>
                            <th class="" width="150">Teléfono 2</th>                          
                            <th class="">Estado</th>
                            <th class="" width="250" >Provincia de nacimiento</th>
                            <th class="" width="150" >Ciudad de nacimiento</th>
                            <th class="">Tipo Sangre</th>                            
                            <th class="" width="250">Email</th>
                            <th class="" width="150">Pais</th>                           
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('assets/template/light/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/template/light/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>    

    <script type="text/javascript">
        var datatableList=$('#listTable').DataTable({
           //"sDom": "<'row mb-1'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6 center'p>>",
           renderer: 'bootstrap',
            serverSide: true,
            processing: true,
            "scrollX": true,
            "responsive": true,
            ajax: "{{route('person.data')}}",
            columns: [
                {data: 'select_action',orderable: false, searchable: false},
                {data: 'ci_tipo_name'},
                {data: 'ci'},                
                {data: 'nombre1'},
                {data: 'nombre2'},
                {data: 'apellido1'},
                {data: 'apellido2'},
                {data: 'sexo_name'},
                {data: 'estado_civil_name'},
                {data: 'fecha_nacimiento'},
                {data: 'domicilio'},
                {data: 'telefono1'},
                {data: 'telefono2'},
                {data: 'estado_info.name'},                
                {data: 'provincia_relation.nombre'},
                {data: 'ciudad_relation.nombre'},                
                {data: 'tipo_sangre'},
                {data: 'email'},
                {data: 'pais_relation.nombre'}                
            ],
            //order: [[ 0 ]],
            lengthMenu:[10,20,50],
            pageLength:10,
            language: {
               processing:     "Procesando ...",
               search:         '<span class="glyphicon glyphicon-search"></span>',
               searchPlaceholder: "BUSCAR",
               lengthMenu:     "Mostrar _MENU_ Registros",
               info:           "Mostrando _START_ a _END_ de _TOTAL_ Registros",
               infoEmpty:      "Mostrando 0 a 0 de 0 Registros",
               infoFiltered:   "(filtrada de _MAX_ registros en total)",
               infoPostFix:    "",
               loadingRecords: "...",
               zeroRecords:    "No se encontraron registros coincidentes",
               emptyTable:     "No hay datos disponibles en la tabla",
               paginate: {
                   first:      "Primero",
                   previous:   "Anterior",
                   next:       "Siguiente",
                   last:       "Ultimo"
               },
               aria: {
                   sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                   sortDescending: ": habilitado para ordenar la columna en orden descendente"
               }
            }
        });
    </script>
    
@endsection
