@extends('layouts.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/template/light/vendors/datatables/css/dataTables.bootstrap.css')}}"/>
@endsection



@section('breadcrumb')
<!-- Content Header (Page header) -->
<section class="content-header">
    <!--section starts-->
    <h1>
        Detalle de Persona
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard.index')}}">
                <i class="fa fa-fw ti-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{route('person.index')}}">Personas</a>
        </li>
        <li class="active">
            <a href="{{route('person.show',$person->id_persona)}}">
                Detalle
            </a>
        </li>
    </ol>
</section>
@endsection

@section('content')
	
	<a class="btn btn-warning pull-right" href="{{route('person.index')}}"><i class="fa fa-fw fa-reply"></i> Regresar</a>
	<div class="clearfix"></div>
    <div class="row" style="margin-top:15px;">
    <div class="col-lg-12">
        <div class="panel ">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-fw fa-angle-double-right"></i> Información de la Persona
                </h3>
                <span class="pull-right hidden-xs">
                {{--<i class="fa fa-fw ti-angle-up clickable"></i>
                <i class="fa fa-fw ti-close removepanel clickable"></i>--}}
            </span>
            </div>
            <div class="panel-body">
              <div class="row">
              	<div class="col-sm-8">
	             
	             <div class="form-group col-sm-6 col-md-6">
	             	<label style="font-weight: bold;">Tipo de Documento</label>
	             	<p>{{$person->ci_tipo_name}}</p>
	             </div>
	             <div class="form-group col-sm-6 col-md-6">
	             	<label style="font-weight: bold;">N° Documento</label>
	             	<p>{{$person->ci}}</p>
	             </div>

	             <div class="form-group col-sm-6 col-md-6">
	             	<label style="font-weight: bold;">Primer Nombre</label>
	             	<p class="text-uppercase">{{$person->nombre1}}</p>
	             </div>
	             <div class="form-group col-sm-6 col-md-6">
	             	<label style="font-weight: bold;">Segundo Nombre</label>
	             	<p class="text-uppercase">{{$person->nombre2}}</p>
	             </div>

	             <div class="form-group col-sm-6 col-md-6">
	             	<label style="font-weight: bold;">Primer Apellido</label>
	             	<p class="text-uppercase">{{$person->apellido1}}</p>
	             </div>
	             <div class="form-group col-sm-6 col-md-6">
	             	<label style="font-weight: bold;">Segundo Apellido</label>
	             	<p class="text-uppercase">{{$person->apellido2}}</p>
	             </div>

	             <div class="form-group col-sm-6 col-md-6">
	             	<label style="font-weight: bold;">Sexo</label>
	             	<p>{{$person->sexo_name}}</p>
	             </div>
	             <div class="form-group col-sm-6 col-md-6">
	             	<label style="font-weight: bold;">Segundo Apellido</label>
	             	<p >{{$person->estado_civil_name}}</p>
	             </div>

	            </div>
	            <div class="col-sm-4">
	               
	                <div class="form-group col-xs-12">
	             		{{--<label style="font-weight: bold;">Foto</label><br>--}}
	             		<img src="{{asset('assets/template/light/img/original.jpg')}}" class="img-responsive img-thumbnail" width="240" >
	             	</div>
	               

	            </div>

	            <div class="col-xs-12">
	            	
	            	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Fecha de Nacimiento</label>
	             		<p>{{$person->fecha_nacimiento}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Estado</label><br>
	             		<p class="label {{$person->estado_info['class']}}">{{$person->estado_info['name']}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Teléfono Principal</label>
	             		<p>{{$person->telefono1}}</p>
	             	</div>

	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Teléfono Segundario</label>
	             		<p>{{$person->telefono2}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-8">
	             		<label style="font-weight: bold;">Domicilio</label>
	             		<p>{{$person->domicilio}}</p>
	             	</div>

	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Pais</label>
	             		<p>{{$person->pais_relation->nombre}}</p>
	             	</div>	             	

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Provincia</label>
	             		<p>{{$person->provincia_relation['nombre']}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Ciudad</label>
	             		<p>{{$person->ciudad_relation['nombre']}}</p>
	             	</div>

	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Tipo de sangre</label>
	             		<p>{{$person->tipo_sangre}}</p>
	             	</div>	             	

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Correo Electrónico</label>
	             		<p>{{$person->email}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">IESS</label>
	             		<p>{{$person->id_iess}}</p>
	             	</div>

	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Dispensario IESS</label>
	             		<p>{{$person->dispensario_IESS}}</p>
	             	</div>	             	

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Licencia</label>
	             		<p>{{$person->licencia_status}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Código de Manejo</label>
	             		<p>{{$person->id_manejo}}</p>
	             	</div>

	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Categoria de Licencia</label>
	             		<p>{{$person->categoria_licencia}}</p>
	             	</div>	             	

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Licencia Caduca</label>
	             		<p>{{$person->licencia_caduca}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Referencia Familiar</label>
	             		<p>{{$person->ref_familiar}}</p>
	             	</div>

	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Referencia Teléfono</label>
	             		<p>{{$person->ref_telefono}}</p>
	             	</div>	             	

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Referencia Domicilio</label>
	             		<p>{{$person->ref_domicilio}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Código Militar</label>
	             		<p>{{$person->id_militar}}</p>
	             	</div>

	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Tiene Asociación</label>
	             		<p>{{$person->tiene_asociacion_status}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Nombre de Asociación</label>
	             		<p>{{$person->asociacion}}</p>
	             	</div>	 

	 				<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Condición</label>
	             		<p>{{$person->condicion}}</p>
	             	</div>
	             	
	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Tipo de discapacidad</label>
	             		<p>{{$person->tipo_discapacidad_name}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Descripción de la Discapacidad</label>
	             		<p>{{$person->discapacidad}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Porcentaje de Discapacidad</label>
	             		<p>{{$person->porcdisc}}%</p>
	             	</div>

	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Codigo de Discapacidad</label>
	             		<p>{{$person->conadis}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">N° Vivienda</label>
	             		<p>{{$person->vivienda}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">N° Alquiler</label>
	             		<p>{{$person->alquiler}}</p>
	             	</div>

	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">N° Hipoteca</label>
	             		<p>{{$person->hipoteca}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Valor Hipoteca</label>
	             		<p>{{$person->valor_hipoteca}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Entidad hipoteca</label>
	             		<p>{{$person->entidad_hipoteca}}</p>
	             	</div>

	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">N° personas</label>
	             		<p>{{$person->no_personas}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Residencia</label>
	             		<p>{{$person->residencia_info['nombre']}}</p>
	             	</div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Familiar discapacitado</label>
	             		<p>{{$person->familiar_discapacitado}}</p>
	             	</div>

	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Discapacitado</label>
	             		<p>{{$person->discapacitado}}</p>
	             	</div>	

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Aprobado</label>
	             		<p>{{$person->aprobado}}</p>
	             	</div>  

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Fecha Aprobacion</label>
	             		<p>{{$person->fecha_aprobacion}}</p>
	             	</div> 

	             	<div class="clearfix"></div>

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Usuario aprobación</label>
	             		<p>{{$person->usuario_aprobacion_info['nombre']}}</p>
	             	</div> 

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Fecha Registro</label>
	             		<p>{{$person->fecha_registro}}</p>
	             	</div>  

	             	<div class="form-group col-sm-4 col-md-4">
	             		<label style="font-weight: bold;">Fecha Modificacion</label>
	             		<p>{{$person->fecha_modificacion}}</p>
	             	</div>         	
	             	
            	
	             	
	            </div>
	          </div>


            </div>

         </div>
    </div>

    </div>


@endsection