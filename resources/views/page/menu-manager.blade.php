<li class="menu-dropdown">
    <a href="javascript:void(0);">
        <i class="menu-icon fa fa-fw fa-angle-double-right"></i>
        <span>
            Usuarios
        </span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
        <li>
            <a href="{{route('user.create')}}">
               <i class="fa fa-fw fa-caret-right"></i> Agregar Usuario
            </a>
        </li>
        <li>
            <a href="{{route('user.index')}}">
                <i class="fa fa-fw fa-caret-right"></i> Listado de Usuarios
            </a>
        </li>                                
    </ul>
</li>
<li class="menu-dropdown">
    <a href="javascript:void(0);">
        <i class="menu-icon fa fa-fw fa-angle-double-right"></i>
        <span>
            Tabla publica
        </span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
        <li>
            <a href="{{route('department.index')}}">
               <i class="fa fa-fw fa-caret-right"></i> Departamentos
            </a>
        </li>
        <li>
            <a href="{{route('city.index')}}">
                <i class="fa fa-fw fa-caret-right"></i> Ciudades
            </a>
        </li>                                
    </ul>
</li>
<li class="menu-dropdown">
    <a href="javascript:void(0);">
        <i class="menu-icon fa fa-fw fa-angle-double-right"></i>
        <span>
            Institución
        </span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
        <li>
            <a href="{{route('institution.create')}}">
               <i class="fa fa-fw fa-caret-right"></i> Agregar Institución
            </a>
        </li>
        <li>
            <a href="{{route('institution.index')}}">
                <i class="fa fa-fw fa-caret-right"></i> Listado de Instituciones
            </a>
        </li>                                
    </ul>
</li>
	