<li class="menu-dropdown">
    <a href="javascript:void(0);">
        <i class="menu-icon fa fa-fw fa-angle-double-right"></i>
        <span>
            Personas
        </span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
        <li>
            <a href="{{route('person.index')}}">
                <i class="fa fa-fw fa-caret-right"></i> Listado
            </a>
        </li>                                
    </ul>
</li>