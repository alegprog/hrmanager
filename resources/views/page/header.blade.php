<!-- header logo: style can be found in header-->
<header class="header">
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="{{route('dashboard.index')}}" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            {{--<img src="{{asset('assets/img/logo_kaval.png')}}" alt="logo"/>--}}
            <b>ONCLOUD</b>
        </a>
        <!-- Header Navbar: style can be found in header-->
        <!-- Sidebar toggle button-->
        <!-- Sidebar toggle button-->
        <div>
            <a href="layout_fixed.html#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <i
                    class="fa fa-fw ti-menu"></i>
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                {{--<li class="dropdown messages-menu">
                    <a href="layout_fixed.html#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw ti-email black"></i>
                        <span class="label label-success">2</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages table-striped">
                        <li class="dropdown-title">New Messages</li>
                        <li>
                            <a href="layout_fixed.html" class="message striped-col">
                                <img class="message-image img-circle" src="{{asset('assets/template/light/img/authors/avatar7.jpg')}}" alt="avatar-image">

                                <div class="message-body"><strong>Ernest Kerry</strong>
                                    <br>
                                    Can we Meet?
                                    <br>
                                    <small>Just Now</small>
                                    <span class="label label-success label-mini msg-lable">New</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="layout_fixed.html" class="message">
                                <img class="message-image img-circle" src="{{asset('assets/template/light/img/authors/avatar6.jpg')}}" alt="avatar-image">

                                <div class="message-body"><strong>John</strong>
                                    <br>
                                    Dont forgot to call...
                                    <br>
                                    <small>5 minutes ago</small>
                                    <span class="label label-success label-mini msg-lable">New</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="layout_fixed.html" class="message striped-col">
                                <img class="message-image img-circle" src="{{asset('assets/template/light/img/authors/avatar5.jpg')}}" alt="avatar-image">

                                <div class="message-body">
                                    <strong>Wilton Zeph</strong>
                                    <br>
                                    If there is anything else &hellip;
                                    <br>
                                    <small>14/10/2014 1:31 pm</small>
                                </div>

                            </a>
                        </li>
                        <li>
                            <a href="layout_fixed.html" class="message">
                                <img class="message-image img-circle" src="{{asset('assets/template/light/img/authors/avatar1.jpg')}}" alt="avatar-image">
                                <div class="message-body">
                                    <strong>Jenny Kerry</strong>
                                    <br>
                                    Let me know when you free
                                    <br>
                                    <small>5 days ago</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="layout_fixed.html" class="message striped-col">
                                <img class="message-image img-circle" src="{{asset('assets/template/light/img/authors/avatar.jpg')}}" alt="avatar-image">
                                <div class="message-body">
                                    <strong>Tony</strong>
                                    <br>
                                    Let me know when you free
                                    <br>
                                    <small>5 days ago</small>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-footer"><a href="layout_fixed.html#">View All messages</a></li>
                    </ul>

                </li>--}}
                <!--rightside toggle-->
                {{--<li>
                    <a href="layout_fixed.html#" class="dropdown-toggle toggle-right">
                        <i class="fa fa-fw ti-view-list black"></i>
                        <span class="label label-danger">9</span>
                    </a>
                </li>--}}
                <!-- User Account: style can be found in dropdown-->
                <li class="dropdown user user-menu">
                    <a href="layout_fixed.html#" class="dropdown-toggle padding-user" data-toggle="dropdown">
                        @if(Auth::check())
                            <img src="{{ auth()->user()->avatar ? asset('file/'.auth()->user()->avatar) : asset('assets/template/light/img/original.jpg') }}" width="35" height="30" class="img-circle pull-left" alt="Imagen">
                        @else
                            <img src="{{ asset('assets/template/light/img/original.jpg') }}" width="35" height="30" class="img-circle img-responsive pull-left" alt="Imagen">
                        @endif
                        <div class="riot">
                            <div>
                                @if(Auth::check())
                                  {{auth()->user()->nombre}}
                                @endif
                                <span>
                                        <i class="caret"></i>
                                    </span>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            @if(Auth::check())
                                <img src="{{ auth()->user()->avatar ? asset('file/'.auth()->user()->avatar) : asset('assets/template/light/img/original.jpg') }}" class="img-circle" alt="Imagen">
                            @else
                                <img src="{{ asset('assets/template/light/img/original.jpg') }}" class="img-circle" alt="Imagen">
                            @endif
                            @if(Auth::check())
                                <p>{{auth()->user()->firstname}}</p>
                            @endif
                        </li>
                        <!-- Menu Body -->
                        <li class="p-t-3">
                            <a href="javascript:void(0);">
                                <i class="fa fa-fw ti-user"></i> Mi Perfil
                            </a>
                        </li>
                        <li role="presentation"></li>
                        {{--<li>
                            <a href="javascript:void(0);">
                                <i class="fa fa-fw ti-settings"></i> Account Settings
                            </a>
                        </li>--}}
                        <li role="presentation" class="divider"></li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                {{--<a href="lockscreen.html">
                                    <i class="fa fa-fa-circle"></i>
                                    Lock
                                </a>--}}
                            </div>
                            <div class="pull-right">
                                @if(Auth::check())              
                                <a href="{{route('getlogout')}}">
                                    <i class="fa fa-fw ti-shift-right"></i>
                                    Cerrar Sesión
                                </a>
                                @endif
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>