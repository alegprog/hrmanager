<!DOCTYPE html>
<html>

<head>
    <title>::Admin Login::</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- Bootstrap -->
    <link href="{{asset('assets/template/light/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- end of bootstrap -->
    <!--page level css -->
    <link type="text/css" href="{{asset('assets/template/light/vendors/themify/css/themify-icons.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/template/light/vendors/iCheck/css/all.css')}}" rel="stylesheet">
    <link href="{{asset('assets/template/light/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}" rel="stylesheet"/>
    <link type="text/css" href="{{asset('assets/template/light/css/app.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/template/light/css/login.css')}}" rel="stylesheet">
     <link rel="stylesheet" href="{{asset('assets/template/light/vendors/animate/animate.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/template/light/vendors/pnotify/css/pnotify.css')}}">
    <link rel="stylesheet" href="{{asset('assets/template/light/vendors/pnotify/css/pnotify.brighttheme.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/template/light/vendors/pnotify/css/pnotify.buttons.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/template/light/vendors/pnotify/css/pnotify.mobile.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/template/light/vendors/pnotify/css/pnotify.history.css')}}" type="text/css"/
    <!--end page level css-->
</head>
<body id="sign-in" >
<div class="preloader">
    <div class="loader_img"><img src="{{asset('assets/template/light/img/loader.gif')}}" alt="Cargando..." height="64" width="64"></div>
</div>
<div class="container">
    <div class="row">
    	@yield('content')
    </div>
</div>
<!-- global js -->
<script src="{{asset('assets/template/light/js/jquery-1.12.4.min.js')}}"></script> 
<script src="{{asset('assets/template/light/js/bootstrap-3.3.7.min.js')}}"></script>
<!-- end of global js -->
<!-- page level js -->
<script type="text/javascript" src="{{asset('assets/template/light/vendors/iCheck/js/icheck.js')}}"></script>
<script src="{{asset('assets/template/light/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.animate.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.buttons.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.confirm.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.nonblock.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.mobile.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.desktop.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.history.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.callbacks.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/template/light/js/custom_js/login.js')}}"></script>
<!-- end of page level js -->

<script type="text/javascript">

		function PNotifyMessage($status,$title,$message){
		    new PNotify({
		        title: $title,
		        text: $message,
		        type: $status,
		        delay: 5000,
		        buttons: {
		          sticker: false,
		          sticker_hover: false,
		          labels: {close: "Cerrar"}
		        },
		        destroy: true,
		    });
		}

		$(document).ready(function() {

			PNotify.prototype.options.styling = "bootstrap3";
		    PNotify.prototype.options.styling = "fontawesome";

			@if(session('success'))
				$message="{{session('success')}}";
	            $status='success';
	            $title='Excelente!';
	            PNotifyMessage($status,$title,$message);
	        @endif


	        @if(count($errors) > 0)
	        	$message='Por favor verifique los datos introducidos';
	            $status='error';
	            $title='Error!';
	            PNotifyMessage($status,$title,$message);
	        @endif

    	});

	</script>


</body>
</html>