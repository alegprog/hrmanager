<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ObonoaITS - Gestión de Nomina</title>
    <meta content='width=device-width, initial-scale=1' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<link rel="shortcut icon" href="{{asset('assets/img/kaval.jpeg')}}"/>--}}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link type="text/css" href="{{asset('assets/template/light/css/app.css')}}" rel="stylesheet"/>
    <!-- end of global css -->
    <!--page level css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/template/light/css/custom.css')}}">

    <link rel="stylesheet" href="{{asset('assets/template/light/css/layouts.css')}}">
    <!--end page level css-->

    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!--end page level css-->

    <link rel="stylesheet" href="{{asset('assets/template/light/vendors/animate/animate.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/template/light/vendors/pnotify/css/pnotify.css')}}">
    <link rel="stylesheet" href="{{asset('assets/template/light/vendors/pnotify/css/pnotify.brighttheme.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/template/light/vendors/pnotify/css/pnotify.buttons.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/template/light/vendors/pnotify/css/pnotify.mobile.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/template/light/vendors/pnotify/css/pnotify.history.css')}}" type="text/css"/>

    <style type="text/css">
        .right-side {
		        margin-top: 50px;
		}
    	@media screen and (max-width: 560px) {
		    .right-side {
		        margin-top: 110px;
		    }
		}
    </style>

    @yield('css')

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
	</script>
    
</head>
<body class="skin-default">
	<div class="preloader">
	    <div class="loader_img"><img src="{{asset('assets/template/light/img/loader.gif')}}" alt="loading..." height="64" width="64"></div>
	</div>

	@include('page.header')

	<div class="wrapper row-offcanvas row-offcanvas-left">
		@include('page.menu-left')
		<aside class="right-side">
			@yield('breadcrumb')
			<section class="content">
				@yield('content')
				<!--rightside bar -->
            	<div id="right">
            		@include('page.menu-right')
            	</div>
            	<div class="background-overlay"></div>
			</section>
			<!-- /.content -->
		</aside>
		<!-- /.right-side -->
	</div>
	<!-- ./wrapper -->
	<!-- global js -->
	{{--<script src="{{asset('js/v5.3/app.js')}}" type="text/javascript"></script>--}}
	
	<script src="{{asset('assets/template/light/js/jquery-1.12.4.min.js')}}"></script> 
	<script src="{{asset('assets/template/light/js/bootstrap_3.3.7-bootstrap.switch_3.3.2-jquery.nicescroll_3.6.0.js')}}"></script> 
	
	<script src="{{asset('assets/template/light/js/app.js')}}" type="text/javascript"></script>
	{{--<script src="{{asset('js/app.js')}}" type="text/javascript"></script>--}}
	<!-- end of global js -->
	<script type="text/javascript" src="{{asset('assets/template/light/js/custom_js/layout_custom.js')}}"></script>

	<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.animate.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.buttons.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.confirm.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.nonblock.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.mobile.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.desktop.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.history.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/template/light/vendors/pnotify/js/pnotify.callbacks.js')}}"></script>

	@yield('js')

	<script type="text/javascript">

		function PNotifyMessage($status,$title,$message){
		    new PNotify({
		        title: $title,
		        text: $message,
		        type: $status,
		        delay: 5000,
		        buttons: {
		          sticker: false,
		          sticker_hover: false,
		          labels: {close: "Cerrar"}
		        },
		        destroy: true,
		    });
		}

		$(document).ready(function() {

			PNotify.prototype.options.styling = "bootstrap3";
		    PNotify.prototype.options.styling = "fontawesome";

			@if(session('success'))
				$message="{{session('success')}}";
	            $status='success';
	            $title='Excelente!';
	            PNotifyMessage($status,$title,$message);
	        @endif


	        @if(count($errors) > 0)
	        	$message='Por favor verifique los datos introducidos';
	            $status='error';
	            $title='Error!';
	            PNotifyMessage($status,$title,$message);
	        @endif

    	});

	</script>

	@yield('script')

</body>

</html>