@extends('layouts.main')

@section('content')
	<div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 login-form">
	        <div class="row">
            <div class="panel-header">
                <h2 class="text-center">
                    <img src="{{asset('assets/img/logo_onoboa.png')}}" width="220" alt="Logo">
                </h2>
            </div>
            <div class="panel-body">
                <div class="row">
                    {{--<div class="col-xs-12" style="padding-left: 30px;padding-right: 30px;">
                        Un producto de
                        <div class="clearfix"></div>
                        <img src="{{asset('assets/img/logo_onoboa.png')}}" width="70" alt="Company">
                    </div>--}}
                    <div class="clearfix"></div>
                    <div style="margin-top: 10px;height: 4px;background-color: #C66B0F; width: 100%"></div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12" style="padding-top: 30px;padding-left: 30px;padding-right: 30px;">                        
                        {!! Form::open(['route' => 'login.post','class'=>'login_validator', 'id'=>"authentication" ]) !!}
                            <div class="form-group">
                                <label for="email" class="sr-only">Usuario</label>
                                <input type="text" class="form-control  form-control-lg" id="username" name="username"
                                       placeholder="Usuario">
                            </div>
                            <div class="form-group">
                                <label for="password" class="sr-only">Contraseña</label>
                                <input type="password" class="form-control form-control-lg" id="password"
                                       name="password" placeholder="Contraseña">
                            </div>
                            {{--<div class="form-group checkbox">
                                <label for="remember">
                                    <input type="checkbox" name="remember" id="remember">&nbsp; Remember Me
                                </label>
                            </div>--}}
                            <div class="form-group text-right">
                                <input type="submit" value="Acceder" class="btn btn-primary"/>
                            </div>
                            {{--<a href="forgot_password.html" id="forgot" class="forgot"> Forgot Password ? </a>

                            <span class="pull-right sign-up">New ? <a href="register.html">Sign Up</a></span>--}}
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="row text-center social">
                    {{--<div class="col-xs-12">
                        <p class="alter">Sign in with</p>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="col-xs-4">
                                <a href="login.html#" class="btn btn-lg btn-facebook">
                                    <i class="ti-facebook"></i>
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="login.html#" class="btn btn-lg btn-twitter">
                                    <i class="ti-twitter-alt"></i>
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="login.html#" class="btn btn-lg btn-google">
                                    <i class="ti-google"></i>
                                </a>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
           </div>
        </div>
@endsection