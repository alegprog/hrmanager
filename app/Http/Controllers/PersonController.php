<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Persona;
use Yajra\Datatables\Datatables;

class PersonController extends Controller
{
    public function index(){
    	return view('dashboard.person.index');
    }

    public function data(Datatables $datatables)
    {
     
        $persons = Persona::with(['provincia' => function ($query) {          
            }])->with(['ciudad' => function ($query) {          
            }])->with(['pais_relation' => function ($query) {          
            }])->get();      

	    return $datatables::of($persons)->addColumn('select_action', function ($person) {
	        return '<span class="pull-right"> <a href="'.route('person.show',$person->id_persona).'"><span style="font-size:18px;" class="fa fa-eye text-info" title="Ver"></span></a></span>';
	    })->rawColumns(['select_action'])->make(true);

    }


    public function show($id){
        $person = Persona::with(['provincia' => function ($query) {          
            }])->with(['ciudad' => function ($query) {          
            }])->with(['pais_relation' => function ($query) {          
            }])->where('id_persona',$id)->firstOrFail(); 

            //dd($person->pais_relation);

        return view('dashboard.person.show',compact('person'));   
    } 
}
