<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout','getLogout']);
    }

    public function showLoginForm(){
        return view('auth.login');
    }

    /*protected function credentials(Request $request)
    {
        //$field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
        //    ? 'codigo'
        //    : 'codigo';

        return [
            'codigo' => $request->username,
            'clave' => $request->password,
        ];
    }*/

    /*public function login(Request $request){

        

        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        //$credentials = $request->only('codigo', 'clave');
        $codigo = $request->username;
        $clave = $request->password;

        //dd($request->all(),$codigo, $clave);

        dd(Auth::attempt(['codigo' => $codigo, 'clave' => bcrypt($clave)]), Auth::attempt(['codigo' => $codigo, 'clave' => $clave]));

        if (Auth::attempt(['codigo' => $codigo, 'clave' => bcrypt($clave)])) {
            // The user is active, not suspended, and exists.
            return redirect()->intended('dashboard');
        }else{
            //return redirect()->back()->with('Error')
            return $this->sendFailedLoginResponse($request);
        }


    }*/

    public function authenticate(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        $codigo = strtoupper($request->username);
        $clave = $request->password;

        $credentials = $request->only('username', 'password');

        //dd(Auth::attempt(['username' => $codigo, 'password' => $clave]), Auth::attempt(['username' => $codigo, 'password' => $request->password]),$codigo,$request->password);

        if (Auth::attempt(['codigo' => $codigo, 'password' => $request->password])) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
