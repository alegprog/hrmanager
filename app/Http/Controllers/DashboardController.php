<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Empleado;
use App\Entities\EmpresaVariable;
use App\Entities\Grupo;
use App\Entities\Empresa;
use App\Entities\Rol;
use App\Entities\VempleadoDirectorio;

class DashboardController extends Controller
{
    public function index(){

    	/*"DECLARE @personal INT 
	   	DECLARE @anio AS CHAR(4)
	   	DECLARE @mes AS CHAR(2)
	   	DECLARE @fechaCorte AS DATE 
	   	
	   	SET @anio = CAST('20' + substring(@rolcodigo,3,2) AS INT)
	   	SET @mes = CAST(substring(@rolcodigo,5,2) AS INT)   	
	   	SELECT @fechaCorte = @anio + '-' + @mes + '-' + dbo.fnc_ultimo_dia_mes(@anio,@mes)
	   	
	   
		SELECT @personal = COUNT(emplecodigo) 
		FROM vempleado_directorio 
		WHERE grupocodigo = @grupo
		AND empcodigo = @empresa
		AND ((estado = 'AC' AND fecha_sal IS NULL) OR 
		     (estado = 'LI' AND fecha_sal > @fechaCorte))"*/

		$rol='RQ171202';
		$empresa='00000001';
		$grupo='00000001';
		$actual=0;
		$anio='';
		$mes='';
		$fechaCorte='';

		$anio='20'.substr($rol, 2, 2); 
		$mes=substr($rol, 4, 2); 
		$fechaCorte=\Carbon\Carbon::parse($anio.'-'.$mes.'-01 00:00:00')->endOfMonth()->toDateString();

		//dd(substr($rol, 2, 2));

		//dd(\Carbon\Carbon::parse($anio.'-'.$mes.'-01 00:00:00')->endOfMonth()->toDateString());

		/*$sql=\DB::table('vempleado_directorio')
		->select(\DB::raw('count(emplecodigo) as personal'))
	    ->whereRaw("grupocodigo='?'",$grupo)
	    ->whereRaw("empcodigo='?'",$empresa)
	    ->WhereRaw("(estado = 'AC' AND fecha_sal IS NULL)")
	    ->orWhereRaw("(estado = 'LI' AND fecha_sal > '?')",$fechaCorte)
	    
	    ->get();*/
	    //->where('estado','=', 'AC')
	    //->groupBy('status')


	    //dd($sql);
	    
	    /*$sql=VempleadoDirectorio::selectRaw('count(emplecodigo) as personal')
	    ->where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)
	    ->WhereRaw("(estado = 'AC' AND fecha_sal IS NULL)")
	    ->orWhereRaw("(estado = 'LI' AND fecha_sal > '".$fechaCorte."')")
	    ->first();
	    dd($sql);*/

	    /*$sql=Empleado::selectRaw('count(emplecodigo) as personal')
	    ->where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)
	    ->WhereRaw("(estado = 'AC' AND fecha_sal IS NULL)")
	    ->orWhereRaw("(estado = 'LI' AND fecha_sal > '".$fechaCorte."')")
	    ->first();*/

	    $empleados_actual=Empleado::with(['persona' => function ($query) {
		    
		}])->where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)	    	    
	    ->WhereRaw("(estado = 'AC' AND fecha_sal IS NULL)")
	    ->orWhereRaw("(estado = 'LI' AND fecha_sal > '".$fechaCorte."')")	    
	    ->get();


	    /*SELECT @valor = CAST(valor AS INT)  
   		FROM empresa_variable 
   		WHERE grupocodigo = @grupo 
   		AND empcodigo = @empresa
   		AND idvariable = 41*/

   		$empresa_variable= EmpresaVariable::select('valor')
   		->where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)
	    ->where("idvariable",41)
	    ->get();

	    /*SELECT @personal = COUNT(emplecodigo) 
		FROM vempleado_directorio 
		WHERE grupocodigo = @grupo
		AND empcodigo = @empresa
		AND year(fecha_ingreso) = @anio
		AND month(fecha_ingreso) = @mes*/	

		$empleado_entrante=Empleado::where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)
	    ->whereYear('fecha_ingreso', $anio)
	    ->whereMonth('fecha_ingreso', $mes)
	    ->count();

	    $empleado_saliente=Empleado::where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)
	    ->whereYear('fecha_sal', $anio)
	    ->whereMonth('fecha_sal', $mes)
	    ->count();

	    $grupos=Grupo::all();
	    $empresas=Empresa::all();
	    $roles=Rol::all();

	    /*$empleado_entrante2=Empleado::where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)
	    ->whereYear('fecha_ingreso', $anio)
	    ->whereMonth('fecha_ingreso', $mes)
	    ->get();*/

	    //dd($empleado_entrante,$empleado_entrante2);


	    $empleados_sexo=collect($empleados_actual)->groupBy('persona.sexo')->toArray();
	    $empleado_discapacidad=collect($empleados_actual)->groupBy('persona.discapacitado')->toArray();

	    //dd($empleado_discapacidad);
	    $total_hombres=isset($empleados_sexo['M']) ? count($empleados_sexo['M']) : 0;
	    $total_mujeres=isset($empleados_sexo['F']) ? count($empleados_sexo['F']) : 0;
	    $total_empleado=$total_hombres+$total_mujeres;
	    $total_sin_discapacidad=isset($empleado_discapacidad['01']) ? count($empleado_discapacidad['01']) : 0;
	    $total_con_discapacidad=isset($empleado_discapacidad['02']) ? count($empleado_discapacidad['02']) : 0;
	    $total_tutores_one=isset($empleado_discapacidad['03']) ? count($empleado_discapacidad['03']) : 0;
	    $total_tutores_two=isset($empleado_discapacidad['04']) ? count($empleado_discapacidad['04']) : 0;
	    $total_tutores_discapacidad=$total_tutores_one+$total_tutores_two;


	    $variable=isset($empresa_variable[0]) ? $empresa_variable[0]->valor : 0; 
	    $valor=$variable != NULL ? $variable : 0;

	    $brecha = $total_empleado / $valor;

	    $total_brecha_discapacidad = $brecha < 0 ? 0 : $brecha;

	    $total_empleado_entrante=$empleado_entrante;
	    $total_empleado_saliente=$empleado_saliente;
	    //$total_tutores_discapacidad=count($empleado_discapacidad['03'] ? $empleado_discapacidad['03'] : 0)+count($empleado_discapacidad['04']);	    

	    //dd($sql,collect($sql)->groupBy('persona.sexo'),$total_hombres,$total_mujeres,$total_empleado);

	    //dd($total_hombres,$total_mujeres,$total_empleado,$total_sin_discapacidad,$total_con_discapacidad,$total_tutores_discapacidad);


	    //dd($total_brecha_discapacidad);

    	return view('dashboard.panel.index',compact('total_sin_discapacidad','total_hombres','total_mujeres','total_empleado','total_con_discapacidad','total_tutores_discapacidad','total_brecha_discapacidad','total_empleado_entrante','total_empleado_saliente','grupos','grupo','empresas', 'empresa','roles', 'rol'));
    }

    public function search(Request $request){

    	//dd($request->all());

    	$rol=$request->rol;
		$empresa=$request->empresa;
		$grupo=$request->grupo;
		$actual=0;
		$anio='';
		$mes='';
		$fechaCorte='';

		$anio='20'.substr($rol, 2, 2); 
		$mes=substr($rol, 4, 2); 
		$fechaCorte=\Carbon\Carbon::parse($anio.'-'.$mes.'-01 00:00:00')->endOfMonth()->toDateString();

		//dd(substr($rol, 2, 2));

		//dd(\Carbon\Carbon::parse($anio.'-'.$mes.'-01 00:00:00')->endOfMonth()->toDateString());

		/*$sql=\DB::table('vempleado_directorio')
		->select(\DB::raw('count(emplecodigo) as personal'))
	    ->whereRaw("grupocodigo='?'",$grupo)
	    ->whereRaw("empcodigo='?'",$empresa)
	    ->WhereRaw("(estado = 'AC' AND fecha_sal IS NULL)")
	    ->orWhereRaw("(estado = 'LI' AND fecha_sal > '?')",$fechaCorte)
	    
	    ->get();*/
	    //->where('estado','=', 'AC')
	    //->groupBy('status')


	    //dd($sql);
	    
	    /*$sql=VempleadoDirectorio::selectRaw('count(emplecodigo) as personal')
	    ->where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)
	    ->WhereRaw("(estado = 'AC' AND fecha_sal IS NULL)")
	    ->orWhereRaw("(estado = 'LI' AND fecha_sal > '".$fechaCorte."')")
	    ->first();
	    dd($sql);*/

	    /*$sql=Empleado::selectRaw('count(emplecodigo) as personal')
	    ->where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)
	    ->WhereRaw("(estado = 'AC' AND fecha_sal IS NULL)")
	    ->orWhereRaw("(estado = 'LI' AND fecha_sal > '".$fechaCorte."')")
	    ->first();*/

	    $empleados_actual=Empleado::with(['persona' => function ($query) {
		    
		}])->where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)	    	    
	    ->WhereRaw("(estado = 'AC' AND fecha_sal IS NULL)")
	    ->orWhereRaw("(estado = 'LI' AND fecha_sal > '".$fechaCorte."')")	    
	    ->get();


	    /*SELECT @valor = CAST(valor AS INT)  
   		FROM empresa_variable 
   		WHERE grupocodigo = @grupo 
   		AND empcodigo = @empresa
   		AND idvariable = 41*/

   		$empresa_variable= EmpresaVariable::select('valor')
   		->where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)
	    ->where("idvariable",41)
	    ->get();

	    //dd($empresa_variable,$grupo,$empresa);

	    /*SELECT @personal = COUNT(emplecodigo) 
		FROM vempleado_directorio 
		WHERE grupocodigo = @grupo
		AND empcodigo = @empresa
		AND year(fecha_ingreso) = @anio
		AND month(fecha_ingreso) = @mes*/	

		$empleado_entrante=Empleado::where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)
	    ->whereYear('fecha_ingreso', $anio)
	    ->whereMonth('fecha_ingreso', $mes)
	    ->count();

	    $empleado_saliente=Empleado::where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)
	    ->whereYear('fecha_sal', $anio)
	    ->whereMonth('fecha_sal', $mes)
	    ->count();

	    $grupos=Grupo::all();
	    $empresas=Empresa::all();
	    $roles=Rol::all();

	    /*$empleado_entrante2=Empleado::where("grupocodigo",$grupo)
	    ->where("empcodigo",$empresa)
	    ->whereYear('fecha_ingreso', $anio)
	    ->whereMonth('fecha_ingreso', $mes)
	    ->get();*/

	    //dd($empleado_entrante,$empleado_entrante2);


	    $empleados_sexo=collect($empleados_actual)->groupBy('persona.sexo')->toArray();
	    $empleado_discapacidad=collect($empleados_actual)->groupBy('persona.discapacitado')->toArray();

	    //dd($empleado_discapacidad);
	    $total_hombres=isset($empleados_sexo['M']) ? count($empleados_sexo['M']) : 0;
	    $total_mujeres=isset($empleados_sexo['F']) ? count($empleados_sexo['F']) : 0;
	    $total_empleado=$total_hombres+$total_mujeres;
	    $total_sin_discapacidad=isset($empleado_discapacidad['01']) ? count($empleado_discapacidad['01']) : 0;
	    $total_con_discapacidad=isset($empleado_discapacidad['02']) ? count($empleado_discapacidad['02']) : 0;
	    $total_tutores_one=isset($empleado_discapacidad['03']) ? count($empleado_discapacidad['03']) : 0;
	    $total_tutores_two=isset($empleado_discapacidad['04']) ? count($empleado_discapacidad['04']) : 0;
	    $total_tutores_discapacidad=$total_tutores_one+$total_tutores_two;


	    $variable=isset($empresa_variable[0]) ? $empresa_variable[0]->valor : 0; 
	    $valor=$variable != NULL ? $variable : 0;

	    $brecha = $total_empleado / $valor;

	    $total_brecha_discapacidad = $brecha < 0 ? 0 : $brecha;

	    $total_empleado_entrante=$empleado_entrante;
	    $total_empleado_saliente=$empleado_saliente;
	    //$total_tutores_discapacidad=count($empleado_discapacidad['03'] ? $empleado_discapacidad['03'] : 0)+count($empleado_discapacidad['04']);	    

	    //dd($sql,collect($sql)->groupBy('persona.sexo'),$total_hombres,$total_mujeres,$total_empleado);

	    //dd($total_hombres,$total_mujeres,$total_empleado,$total_sin_discapacidad,$total_con_discapacidad,$total_tutores_discapacidad);


	    //dd($total_brecha_discapacidad);

    	return view('dashboard.panel.index',compact('total_sin_discapacidad','total_hombres','total_mujeres','total_empleado','total_con_discapacidad','total_tutores_discapacidad','total_brecha_discapacidad','total_empleado_entrante','total_empleado_saliente','grupos','grupo','empresas', 'empresa','roles', 'rol'));
    }
}
