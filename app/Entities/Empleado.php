<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $table='empleados';

    //protected $table='empleado';

    protected $dates=['fecha_sal'];


    public function persona(){
    	return $this->belongsTo('App\Entities\Persona','id_persona','id_persona');
    }
}
