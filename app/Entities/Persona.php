<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;


class Persona extends Model
{
    //protected $table='personas';

    protected $table='personas';

    protected $appends=['ci_tipo_name','sexo_name', 'estado_civil_name','ciudad_relation','provincia_relation', 'licencia_status','tiene_asociacion_status', 'estado_info', 'residencia_info', 'usuario_aprobado_info'];

    public function getCiTipoNameAttribute(){
    	return $this->ci_tipo == 'C' ? 'Cedula' : 'Pasaporte';
    }

    public function getSexoNameAttribute(){
    	return $this->sexo == 'M' ? 'Masculino' : 'Femenino';
    }

    public function getEstadoCivilNameAttribute(){
    	
    	if($this->estado_civil=='C'){
    		return 'Casado';
    	}elseif($this->estado_civil=='S'){
    		return 'Soltero';
    	}elseif($this->estado_civil=='V'){
    		return 'Viudo';
    	}elseif($this->estado_civil=='U'){
    		return 'Unión Libre';
    	}
    }

    public function getLicenciaStatusAttribute(){
        return $this->licencia == '1' ? 'Si' : 'No';
    }

    public function getTieneAsociacionStatusAttribute(){
        return $this->tiene_asociacion == 'S' ? 'Si' : 'No';
    }

    public function getTipoDiscapacidadNameAttribute(){
        
        if($this->tipo_discapacidad=='M'){
            return 'Mental';
        }elseif($this->tipo_discapacidad=='F'){
            return 'Fisica';
        }elseif($this->tipo_discapacidad=='O'){
            return 'Otra';
        }else{
            return '';
        }
        
    }

   public function getEstadoInfoAttribute(){
        
        if($this->estado=='AC'){
            return ['name'=>'Activo','class'=>'label-success'];
        }elseif($this->estado=='IN'){
            return ['name'=>'Inactivo','class'=>'label-danger'];
        }elseif($this->estado=='LI'){
            return ['name'=>'Liquidado','class'=>'label-info'];
        }elseif($this->estado=='PE'){
            return ['name'=>'Pendiente','class'=>'label-warning'];
        }else{
            return ['name'=>'','class'=>'label-default'];
        }
        
        
    }
    

    public function provincia(){
        return $this->belongsTo('App\Entities\Provincia','provincia_nacimiento', 'provcodigo');
    }

    public function ciudad(){
        return $this->belongsTo('App\Entities\Ciudad','ciudad_nacimiento', 'ciucodigo');
    }

    public function getCiudadRelationAttribute(){
    	return $this->ciudad ? $this->ciudad : ['nombre'=>''];
    }

    public function getProvinciaRelationAttribute(){
    	return $this->provincia ? $this->provincia : ['nombre'=>''];
    }

    public function pais_relation(){
        return $this->belongsTo('App\Entities\Tipo','pais', 'codigo')->where('tipo','PAI');
    }

    public function residencia_relation(){
        return $this->belongsTo('App\Entities\Tipo','residencia', 'codigo')->where('tipo','PAI');
    }

    public function usuario_aprobado_relation(){
        return $this->belongsTo('App\User','usuario_aprobado', 'codigo');
    }

    public function getUsuarioAprobadoInfoAttribute(){
        return $this->usuario_aprobado_relation ? $this->usuario_aprobado_relation : ['nombre'=>''];
    }

    public function getResidenciaInfoAttribute(){
        return $this->residencia_relation ? $this->residencia_relation : ['nombre'=>''];
    }

}
